<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name') }}</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css?v=1234" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/datatables.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/materialize.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/slot-machine.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
  </head>

  <body>
    <div id="bodyCover"></div>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-43729810-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-43729810-1');
    </script>
    <!-- End Global site tag (gtag.js) - Google Analytics -->

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s) {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '1827202500843084');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1827202500843084&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->

    <nav class="navbar navbar-expand-lg py-lg-0" id="navigation">
      <!--<a class="navbar-brand" href="#">Navbar</a> -->
      <button id="toggle-navbar" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
        <div class="bar-1"></div>
        <div class="bar-2"></div>
        <div class="bar-3"></div>
      </button>

      <div class="collapse navbar-collapse" id="main-navbar">
        <ul class="navbar-nav mx-auto">
          <li class="nav-item active">
            <a class="nav-link" href="/">Početna <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('events') }}">Prijava</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/nagrade">Nagrade</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/kontakt">Kontakt</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/pravila">Pravila</a>
          </li>
        </ul>
      </div>
    </nav>

    <div id="mobile-navbar">
      <ul class="nav navbar-nav">
        <li class="nav-item">
          <a href="/" class="nav-link">Početna <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('events') }}">Prijava</a>
        </li>
        <li class="nav-item">
          <a href="/nagrade" class="nav-link">Nagrade</a>
        </li>
        <li class="nav-item">
          <a href="/kontakt" class="nav-link">Kontakt</a>
        </li>
        <li class="nav-item">
          <a href="/pravila" class="nav-link">Pravila</a>
        </li>
      </ul>
    </div>

    <div class="container-fluid">
      <div class="row">
      </div>
    </div>

    <script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('/js/popper.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.js') }}"></script>
    <script src="{{ asset('/js/datatables.js') }}"></script>
    <script src="{{ asset('/js/materialize.js') }}"></script>
    @yield('javascript')
    <script>
    $(document).ready(function() {
      document.getElementById("toggle-navbar").addEventListener("click", function() {
        document.getElementById("toggle-navbar").classList.toggle("change");
        document.getElementById("mobile-navbar").classList.toggle("show");
        document.getElementById("navigation").classList.toggle("bg_orange");
        
      });

      var url = window.location.href;
      var array = url.split('/');
      var lastPart = array[array.length-1];
      switch (lastPart) {
        case '':
          document.querySelectorAll('#mobile-navbar ul li')[0].classList.add('active');
          break;
        case 'nagrade':
          document.querySelectorAll('#mobile-navbar ul li')[1].classList.add('active');
          break;
        case 'dobitnici':
          document.querySelectorAll('#mobile-navbar ul li')[2].classList.add('active');
          break;
        case 'kontakt': 
          document.querySelectorAll('#mobile-navbar ul li')[3].classList.add('active');
          break;
        case 'pravila':
          document.querySelectorAll('#mobile-navbar ul li')[4].classList.add('active');
          break;
      }
    });
    </script>
  </body>
</html>
