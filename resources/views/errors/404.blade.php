@extends('layouts.apperror')

@section('content')



<div class="container error-container">
  <div class="error-parent">
    <h1 class="display-4">Greška</h1>
    <img src="{{ asset('/img/404.png') }}" alt="404">
    <p>Žao nam je, tražena stranica ne postoji.</p>
  </div>
</div>

@endsection
