@extends('layouts.app')

@section('content')
  <div class="container main-pravila-container">

  <div class="row">
    <div class="container pravila-container">
      <h1 class="pravila-title">Pravila</h1>
      <p class="text-white text-left rules_head__content">Na osnovu Zakona o igrama na sreću (Službeni glasnik RS, br. 88/2011 i 93/2012 – dr. zakon,30/2018, 95/2018 i 91/2019), Pravilnika o bližim uslovima, odnosno sadržini pravila igara na sreću(Službeni glasnik RS, broj 129/04) i Odluke direktora Ušće Shopping Center d.o.o. Beograd br. 65/01-20 od 7. februara 2020. godine, utvrđujemo:</p>
      <span class="text-uppercase rules_head__title">Pravila nagradne igre</span>
      <h2 class="text-white text-uppercase rules_title">"Ušće rođendanski match"</h2>
      <div class="rules_list__container">
        <ul class="rules_list">
          <li class="text-white">Član 1.</li>
          <li class="text-white">Priređivač nagradne igre</li>
        </ul>
        <ul class="rules_list">
          <li class="text-white">Priređivač nagradne igre "Ušće rođendanski match" je UŠĆE SHOPPING CENTER D.O.O.BEOGRAD, Bulevar Mihajla Pupina 6, 11070 Beograd, matični broj 20567716, PIB 106272110 (udaljem tekstu: "Priređivač"). Nagradna igra će biti organizovana u objektu Ušće Shopping Centar, naadresi Bulevar Mihajla Pupina 4, 11070 Beograd (u daljem tekstu: "Ušće Shopping Centar").</li>
        </ul>
        <ul class="rules_list">
          <li class="text-white">Član 2.</li>
          <li class="text-white">Vrsta nagradne igre</li>
        </ul>
        <ul class="rules_list">
          <li class="text-white">Nagradna igra u robi u uslugama.</li>
        </ul>
        <ul class="rules_list">
          <li class="text-white">Član 3.</li>
          <li class="text-white">Zvanični naziv nagradne igre</li>
        </ul>
        <ul class="rules_list">
          <li class="text-white">Zvanični naziv nagradne igre je “Ušće rođendanski match”.</li>
        </ul>
        <ul class="rules_list">
          <li class="text-white">Član 4.</li>
          <li class="text-white">Pravo učešća</li>
        </ul>
        <ul class="rules_list">
          <li class="text-white">Pravo učešća u nagradnoj igri imaju sva punoletna fizička lica koja prihvataju Pravila nagradne igre ikoja su izvršila kupovinu proizvoda i/ili usluga u Ušće Shopping Centru u visini:
• od i preko 5.000,00 dinara sa najviše dva računa.</li>
          <li class="text-white">U nagradnoj igri ne mogu učestvovati: maloletna lica, lica koja nisu državljani Republike Srbije, zaposleni kod Priređivača, angažovani u kompanijama koje učestvuju u organizaciji nagradne igre na bilo koji način, kao i njihovi najbliži članovi porodice: roditelji, deca i supružnici navedenih lica.</li>
        </ul>
        <ul class="rules-list">
          <li class="text-white">Član 5.</li>
          <li class="text-white">Trajanje nagradne igre</li>
        </ul>
        <ul class="rules-list">
          <li class="text-white">Nagradna igra počinje 16. marta 2020. godine i traje do 12. aprila 2020. godine.</li>
        </ul>
        <ul class="rules-list">
          <li class="text-white">Član 6.</li>
          <li class="text-white">Pravila učešća u nagradnoj igri</li>
        </ul>
        <ul class="rules-list">
          <li class="text-white">Pravo učešća u nagradnoj igri imaju lica navedena u članu 4. ukoliko u periodu trajanja nagradne igre, odnosno u periodu od 16. marta do 12. aprila 2020. godine izvrše sledeće radnje:</li>
          <li class="text-white">1) izvrše kupovinu proizvoda i/ili usluga u Ušće Shopping Centru u vrednosti:</li>
          <li class="text-white">• od i preko 5.000,00 dinara sa najviše dva računa;</li>
          <li class="text-white">2) samostalno ili uz pomoć promotera koji će svakodnevno tokom perioda nagradne igre biti prisutni u Ušće Shopping Centru, izvrše registraciju svojih podataka na internet stranici nagradne igre <strong>11godina.usceshoppingcenter.rs </strong> na kojoj je potrebno: </li>
          <li class="text-white">(A) upišu svoje ime i prezime, email adresu, broj telefona, zatim broj isečka (BI) računa sa kojima učestvuju u nagradnoj igri (maksimum dva) i naziv radnje u Ušće Shopping Centru u kojoj je izvršena kupovina i dobijeni računi, kao i iznos računa, nakon čega će dobiti jedinstveni jedanaestocifreni registracioni broj (u daljem tekstu:  <strong>"Registracioni broj"</strong>) ) koji će automatski biti i poslat na ostavljenu e-mail adresu učesnika.</li>
          <li class="text-white">Račun(i) se mogu samo jednom koristiti za učestvovanje u nagradnoj igri, te će bilo kakvo višestruko korišćenje računa, odnosno BI broja, odnosno brojeva biti onemogućeno. Ukoliko se eventualno naknadno pokaže da je jedan račun više puta učestvovao u nagradnoj igri, takav račun i učesnik, odnosno učesnici će biti diskvalifikovani iz nagradne igre. </li>
          <li class="text-white">(B) klikom na označeno polje:</li>
          <li class="text-white">(i) saglase sa Pravilima nagradne igre koja će učesnicima biti na uvidu u štampanom materijalu u Ušće Shopping Centru i biti dostupna na internet stranici nagradne igre, (ii) potvrđuju da nisu angažovani kod Priređivača odnosno u kompanijama koje učestvuju u organizaciji nagradne igre na bilo koji način, kao i da nisu član uže porodice bilo kog takvog zaposlenog, (iii) saglase se davanjem i obradom ličnih podataka od strane Priređivača u skladu sa važećim Zakonom o zaštiti podataka o ličnosti, koji se budu zahtevali u skladu sa ovim Pravilima; i (iv) saglase se da će ukoliko budu dobitnik Instant nagrade njihovo ime i prezime i Registracioni brojevi biti objavljeni na internet stranici nagradne igre i ukoliko su dobitnici Poklon vaučera za robu i usluge-kartice za dobitak od 111.000,00 dinara i Poklon vaučera za Nagradna putovanja i Glavne nagrade njihova imena i prezimena biti objavljena i u dnevnim novinama koje se distribuiraju na celoj teritoriji Republike Srbije, najkasnije 15 dana nakon završetka nagradne igre, kao i na internet stranici nagradne igre i društvenim mrežama Ušće Shopping Centra, i da će se njihovo ime i prezime, fotografije i/ili snimljeni materijal koristiti na reklamno/promotivnim materijalima na svim medijima, isključivo u svrhe priređivanja i promovisanja nagradne igre, sve u skladu sa ovim Pravilima nagradne igre i u skladu sa članom 14. i 15. Zakona o zaštiti podataka o ličnosti.</li>
          <li class="text-white">(3) kliknu na označeno polje na sledećoj stranici na kojoj će se prikazati tri polja na kojima se nalaze brojevi od 0 do 9, te ukoliko se na svih tri polja nalaze brojevi (3/3/3) ili (6/6/6) slučajnim elektronskim izborom učesnik osvaja jednu od Instant nagrada i to: (i) poklon vaučer za robe i usluge-kartice na iznos od 3.000,00 dinara (ukoliko su se poklopili brojevi 3/3/3) (ukupno raspoloživih - 111), (ii) poklon vaučer za robe i usluge-kartice na iznos od 6.000,00 dinara (ukoliko su se poklopili brojevi 6/6/6) (ukupno raspoloživih - 111), sve za potrošnju u Ušće Shopping Centru; ili </li>
          <li class="text-white">(4) ukoliko se na sva tri polja ne poklone isti brojevi kako je prethodno opisano, učesnik automatski stiče pravo učešća u izvlačenju tzv. Poklon vaučera za robu i usluge-kartice za dobitak od 111.000,00 dinara za potrošnju u Ušće Shopping Centru (ukupno raspoloživih - 8).; ili  </li>
          <li class="text-white">(5) ukoliko učesnik nije ostvario dobitak u skladu sa tačkom (3) ili (4), učesnik automatski stiče pravo učešća u izvlačenju Poklon vaučera za Nagradno putovanje u turističkoj agenciji PANORAMA DOO BEOGRAD (ukupno raspoloživih - 2) i to: </li>
          <li class="text-white">• poklon vaučer za nagradno putovanje u Madrid, na ATP finale (finale Asocijacije teniskih profesionalca) za najviše dve osobe periodu od 5. do 12. maja 2020. godine u vrednosti 357.472,00‬ dinara, a koji vaučer uključuje plaćeni smeštaj u trajanju 7 noći/8 dana u hotelu 3* na bazi usluge noćenje sa doručkom i povratnim avionskim kartama, transportom aerodrom – hotel – aerodrom, troškovima taksi i osiguranja i troškovima dve ulaznice za ATP finale (ukupno raspoloživih – 1 poklon vaučer); ili</li>
          <li class="text-white">•  poklon vaučer za nagradno putovanje u London na "London Fashion Week" za najviše dve osobe u periodu od 17. do 24. septembra 2020. godine, u vrednosti 578.542,00 dinara a koji vaučer uključuje plaćeni smeštaj u trajanju 7 noći/8 dana u hotelu 3* na bazi usluge noćenje sa doručkom i povratnim avionskim kartama, transportom aerodrom – hotel – aerodrom, troškovima viziranja, taksi i osiguranja i troškovima dve ulaznice za "London Fashion Week" za određene revije (ukupno raspoloživih – 1 poklon vaučer); ili  </li>
          <li class="text-white">(6) ukoliko pak učesnik nije ostvario dobitak u skladu sa tačkom (3), (4) ili (5) učesnik automatski stiče pravo učešća u izvlačenju Glavne nagrade – poklon vaučer u vrednosti 698.482,00 dinara u turističkoj agenciji PANORAMA DOO BEOGRAD za najviše dve osobe za putovanje u Los Anđeles i Holivud, u periodu po izboru do 31. oktobra 2020. godine a koji vaučer uključuje plaćeni smeštaj u trajanju 7 noći/8 dana u hotelu 3* na bazi usluge noćenje sa doručkom i povratnim avionskim kartama, transportom aerodrom – hotel – aerodrom, troškovima viziranja, taksi i osiguranja, troškovima izleta za 2 osobe (Walk of Fame; Universal studios; Warner Bros studio; Disneyland; hop on hop off autobus 24 sata) (ukupno raspoloživih – 1 poklon vaučer).</li>
          <li class="text-white">Svaki učesnik može osvojiti samo jednu nagradu, te osvajanje jedne nagrade isključuje mogućnost i automatski diskvalifikuje učesnika iz daljeg učestvovanja u nagradnoj igri. </li>
          <li class="text-white">Registracija na internet stranici nagradne igre i izvlačenje Instant nagrada kako je gore opisano je moguće svakog dana tokom trajanja Nagradne igre, u periodu od 10.00 časova do 22.00 časa. </li>
            <li class="text-white">Ne postoji dnevno ograničenje broja Instant nagrada koje se dodeljuju učesnicima, već samo opšte ograničenje do isteka zaliha, odnosno dok se ne izvuku sve Instant nagrade.  </li>
            <li class="text-white">Izvlačenje Poklon vaučera za robu i usluge-kartice za dobitak od 111.000,00 dinara, Poklon vaučera za Nagradno putovanje i Glavne nagrade će se odvijati putev slučajnog elektronskog odabira Registracionog broja na elektronskom brojčaniku koji će se nalaziti na ekranu u Ušće Shopping Centru, uz mogućnost praćenja na Facebook profilu Priređivača. </li>
            <li class="text-white">Izvlačenje po jednog Poklon vaučer za robu i usluge-kartice za dobitak od 111.000,00 dinara izvlačiće se sledećim danima u 18.00 časova: 18. marta, 22. marta, 25. marta, 29. marta, 1. aprila, 5. aprila, 8. aprila i 12. aprila 2020. godine. </li>
          <li class="text-white">Izvlačenje po jednog Poklon vaučera za Nagradno putovanje izvlačiće se sledećim danima u 18.00 časova: 22. marta i 5. aprila 2020. godine. </li>
          <li class="text-white">Izvlačenje Glavne nagrade će se obaviti na dan završetka nagradne igre, odnosno 12. aprila 2020. godine u 18.00 časova. </li>
          <li class="text-white">Da bi učesnik stekao pravo na učestvovanje u izvlačenju Poklon vaučer za robu i usluge-kartice za dobitak od 111.000,00 dinara, Poklon vaučera za Nagradno putovanje i Glavne nagrade, mora da izvrši registraciju na internet stranici nagradne igre najkasnije 10 minuta pre izvlačenja (odnosno do 17.50 časova) na dan izvlačenja njihovog izvlačenja. </li>
          <li class="text-white">Svi dobitnici nagrada će biti informisani o rezultatu putem telefonskog poziva i na email adresu koje su ostavili kao kontakt na internet stranici nagradne igre, kako je gore navedeno. Pored navedenog, svi dobitnici Instant nagrada (odnosno njihova imena i prezimena i Registracioni brojevi) će tokom perioda Nagradne igre i 15 dana po prestanku Nagradne igre biti objavljeni u posebnoj tabeli dobitnika na internet stranici nagradne igre, kako bi učesnici u svakom trenutku mogli da provere da li su dobitnici ovih nagrada. </li>
          <li class="text-white">Korišćenje bilo kakve vrste kompjuterskog koda, virusa, ili bilo čega što može ometati, onemogućiti, naneti štetu, ili zloupotrebiti nagradnu igru je zabranjeno. Takvi postupci će dovesti do eliminacije učesnika iz nagradne igre. Ukoliko se proceni, ili posumnja da neki od učesnika na bilo koji način krši Pravila nagradne igre, Priređivač zadržava pravo da ih diskvalifikuje iz nagradne igre.</li>
          <li class="text-white">Priređivač ne preuzima i ne snosi odgovornost za:</li>
          <li class="text-white">-	bilo koje posledice koje su nastale kao rezultat zloupotrebe sadržaja i zloupotrebe sistema funkcionisanja nagradne igre na štetu učesnika, dobitnika ili trećeg lica;</li>
          <li class="text-white">-	(ne)istinitost podataka koje učesnici ostavljaju;</li>
          <li class="text-white">-	posledice koje nastaju usled netačnih ili pogrešnih ili neovlašćenih podataka koje je dao učesnik, odnosno za neuspeh ili sprečenost u realizaciji isporučivanja nagrada zbog nedostataka/grešaka u podacima koje je dao učesnik (npr. pogrešno ime ili prezime) odnosno za štete, koje su proistekle iz tih razloga.</li>
          <li class="text-white">Priređivač ne snosi odgovornost zbog kašnjenja prouzrokovanih smetnjama u funkcionisanju samog sajta. Povremeno može da se dogodi da se usluga ne može koristiti iz razloga koji ne zavise od Priređivača. Prihvatanjem uslova učesnik prima k znanju da sadržaj, mogućnost, brzina prenosa poruka, podataka i odgovora zavisi od tehničkih mogućnosti provajdera, i time na ove elemente može nepovoljno uticati opterećenost mreže, i drugi faktori, koji su izvan kontrole Priređivač, kao što su (ne i isključivo) zagušenost mreže, pokrivenost, greške u konekciji kao i održavanje bezbedne konekcije na mrežu i vebsajt. Učešće u nagradnoj igri zavisi i od kapaciteta servera, odnosno zasićenosti mreže, pa shodno tome gore navedene okolnosti povremeno mogu negativno uticati na pristup nagradnoj igri.</li>
          <li class="text-white">Priređivač ne snosi odgovornost zbog tehničkih grešaka ili grešaka u unosu podataka u toku funkcionisanja nagradne igre i drugih razloga, koji ne zavise od Priređivača.</li>
        </ul>
        <ul class="rules-list">
          <li class="text-white text-center">Član 7.</li>
          <li class="text-white text-center">Nagrade i nagradni fond</li>
        </ul>
        <ul class="rules-list">
          <li class="text-white">U nagradnoj igri "Ušće rođendanski match", biće dodeljene sledeće nagrade sa sledećom specifikacijom i vrednošću:</li>
        </ul>
        <table class="table table-responsive">
          <thead class="thead-dark">
            <tr>
              <th>Naziv nagrade</th>
              <th>Broj nagrada</th>
              <th>Neto cena pojedinačne nagrade u RSD</th>
              <th>Bruto cena pojedinačne nagrade u RSD</th>
              <th>Neto vrednost nagradnog fonda u RSD</th>
              <th>Bruto vrednost nagradnog fonda u RSD</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>Poklon vaučer za robe i usluge – kartica za potrošnju u Ušće Shopping Centru na iznos od 3.000,00 dinara
                (Instant nagrada)</th>
              <td>111</td>
              <td>3.000,00</td>
              <td>3.000,00</td>
              <td>333.000,00</td>
              <td>333.000,00</td>
            </tr>
            <tr>
              <th>Poklon vaučer za robe i usluge – kartica za potrošnju u Ušće Shopping Centru na iznos od 6.000,00 dinara
                (Instant nagrada)</th>
              <td>111</td>
              <td>6.000,00</td>
              <td>6.000,00</td>
              <td>666.000,00</td>
              <td>666.000,00</td>
            </tr>
            <tr>
              <th>dobitak u visini od 111.000,000 dinara (u vidu nekoliko poklon vaučera za robe i usluge-kartice u različitim vrednostima, ali ukupno u zbiru od 111.000,00 dinara
                (Poklon vaučer za robu i usluge-kartice za dobitak od 111.000,00 dinara)</th>
              <td>8</td>
              <td>111.000,00</td>
              <td>111.000,00</td>
              <td>888.000‬,00</td>
              <td>888.000‬,00</td>
            </tr>
            <tr>
              <th>poklon vaučer za nagradno putovanje u Madrid, na ATP finale (finale Asocijacije teniskih profesionalca) za najviše dve osobe periodu od 5. do 12. maja 2020. godine u vrednosti 357.472,00‬ dinara, a koji vaučer uključuje plaćeni smeštaj u trajanju 7 noći/8 dana u hotelu 3* na bazi usluge noćenje sa doručkom i povratnim avionskim kartama, transportom aerodrom – hotel – aerodrom, troškovima taksi i osiguranja i troškovima dve ulaznice za ATP finale
                (Poklon vaučer za Nagradno putovanje)</th>
              <td>1</td>
              <td>357.472,00</td>
              <td>357.472,00</td>
              <td>357.472,00</td>
              <td>357.472,00</td>
            </tr>
            <tr>
              <th>poklon vaučer za nagradno putovanje u London na "London Fashion Week" za najviše dve osobe u periodu od 17. do 24. septembra 2020. godine, u vrednosti 578.542,00 dinara a koji vaučer uključuje plaćeni smeštaj u trajanju 7 noći/8 dana u hotelu 3* na bazi usluge noćenje sa doručkom i povratnim avionskim kartama, transportom aerodrom – hotel – aerodrom, troškovima viziranja, taksi i osiguranja i troškovima dve ulaznice za "London Fashion Week" za određene revije
                (Poklon vaučer za Nagradno putovanje)</th>
              <td>1</td>
              <td>578.542,00</td>
              <td>578.542,00</td>
              <td>578.542,00</td>
              <td>578.542,00</td>
            </tr>
            <tr>
              <th>poklon vaučer u vrednosti 698.482,00 dinara u turističkoj agenciji PANORAMA DOO BEOGRAD za najviše dve osobe za putovanje u Los Anđeles i Holivud, u periodu po izboru do 31. oktobra 2020. godine a koji vaučer uključuje plaćeni smeštaj u trajanju 7 noći/8 dana u hotelu 3* na bazi usluge noćenje sa doručkom i povratnim avionskim kartama, transportom aerodrom – hotel – aerodrom, troškovima viziranja, taksi i osiguranja, troškovima izleta za 2 osobe (Walk of Fame; Universal studios; Warner Bros studio; Disneyland; hop on hop off autobus 24 sata) (Glavna nagrada)</th>
              <td>1</td>
              <td>698.482,00</td>
              <td>698.482,00</td>
              <td>698.482,00</td>
              <td>698.482,00</td>
            </tr>
            <tr style="background-color: #343a40;">
              <th>Ukupno:</th>
              <td>233</td>
              <td>1.754.496,00‬</td>
              <td>1.754.496,00‬</td>
              <td>3.521.496,00</td>
              <td>.521.496,00</td>
            </tr>
          </tbody>
        </table>
        <ul class="rules-list">
          <li class="text-white">Instant nagrade i Poklon vaučer za robu i usluge-kartice za dobitak od 111.000,00 dinara (u vidu nekoliko poklon vaučera za robe i usluge-kartice u različitim vrednostima, ali ukupno u zbiru od 111.000,00 dinara) se mogu preuzeti počev od narednog dana od dana osvajanja i biće biti aktivne počev od dana aktiviranja tj. dana uručenja dobitniku na promotivnom pultu u Ušće Shopping Centru, pa sve do 31. decembra 2020. godine i nakon proteka tog roka ne mogu se više koristiti.</li>
          <li class="text-white">U cilju izbegavanja nedoumica, sve Instant nagrade i Poklon vaučer za robu i usluge-kartice za dobitak od 111.000,00 dinara se mogu koristi isključivo za kupovinu roba (tekstil, aksesoar, tehnika, računari, multimediji, prehrambeni proizvodi, kozmetika, nameštaj i ostala roba široke potrošnje) i usluga (ugostiteljstvo, bioskop, frizer, itd.) u Ušće Shopping Centru i to do njihovog iskorišćavanja do punog iznosa i isteka njihovog važenja. </li>
          <li class="text-white">Poklon vaučeri za Nagradno putovanje i Glavna nagrada će biti aktivni počev od dana njihovog uručenja u Ušće Shopping Centru, koje uručenje će biti izvršeno u roku od 7 (sedam) radnih dana od dana njihovog izvlačenja. Poklon vaučer za Nagradno putovanje u Madrid se isključivo može koristi u periodu od 5. do 12. maja 2020. godine, Poklon vaučer za Nagradno putovanje u London se može iskoristiti samo u periodu od 17. do 24. septembra 2020. godine, a Glavna nagrada, odnosno poklon vaučer za putovanje u Los Anđeles i Holivud se može iskoristiti do 31. oktobra 2020. godine.</li>
          <li class="text-white">Ukoliko iz bilo kojih razloga dobitnik ne bude u mogućnosti da iskoristi Poklon vaučer za Nagradno putovanje ili Glavnu nagradu, Priređivač neće imati bilo kakve obaveze prema takvom dobitniku, odnosno takav dobitnik neće imati pravo na bilo kakvu naknadu od Priređivača. Ukoliko dobitnik Poklon vaučera za Nagradno putovanje ili Glavne nagrade ne iskoristi vaučer u celosti neće imati prava na bilo kakvu razliku do punog iznosa vaučera.</li>
          <li class="text-white">Ukoliko dobitnik Poklon vaučer za Nagradno putovanje ili Glavne nagrade ne želi da prihvati nagradu, može je preneti na drugo lice isključivo i najkasnije do momenta isteka roka za uručenje ove nagrade, ali je u obavezi da Priređivaču dostavi pisanu saglasnost da se takva nagrada prenese na drugo lice, uz tačno navođenje podataka o licu na koje se nagrada prenosi, u kom slučaju će lice na koje se nagrada prenosi biti dužno da dostavi iste podatke koje u skladu sa ovim Pravilima nagradne igre treba da dostavi dobitnik nagrade koja se prenosi. Takođe, sve odredbe ovih Pravila nagradne igre će se primenjivati i na to lice koje stupa na mesto dobitnika po osnovu prenosa Poklon vaučera za Nagradno putovanje ili Glavne nagrade, te je lice na koje se prenosi dužno da pre preuzimanja Poklon vaučer za Nagradno putovanje ili Glavne nagrade potpiše i izjavu kojom se saglašava sa pravima i obavezama iz člana 6, tačka B) ovih Pravila.</li>
          <li class="text-white">Ni jedna nagrada se ne može zameniti, unovčiti, odnosno iskoristiti za podizanje novca sa bankomata, niti dopunjavati od strane dobitnika. Instant nagrade, Poklon vaučer za robu i usluge-kartice za dobitak od 111.000,00 dinara se ne mogu prenositi dok se Poklon vaučeri za Nagradno putovanje i Glavna nagrada mogu prenositi samo kako je navedeno u prethodnom stavu.</li>
        </ul>
        <ul class="rules-list">
          <li class="text-white text-center">Član 8.</li>
          <li class="text-white text-center">Preuzimanje nagrada, nadzor i objava dobitnika</li>
        </ul>
        <ul class="rules-list">
          <li class="text-white">Priređivač garantuje za isplatu dobitaka u nagradnoj igri, u skladu sa članom 23. Zakona o igrama na sreću.</li>
          <li class="text-white">Dobitnici nagrada će svoje nagrade preuzeti kako sledi:</li>
          <li class="text-white">1) Instant nagrade dobitnik može da preuzme na promotivnom pultu u Ušće Shopping Centru počev od narednog dana od dana osvajanja nagrade, a u roku od 7 (sedam) dana od dana izvlačenja nagrade;</li>
          <li class="text-white">2) Poklon vaučere za robu i usluge-kartice za dobitak od 111.000,00 dinara, dobitnik može da preuzme na promotivnom pultu u Ušće Shopping Centru počev od narednog dana od dana osvajanja nagrade, a u roku od 7 (sedam) dana od dana izvlačenja nagrade;</li>
          <li class="text-white">3) Poklon vaučeri za Nagradno putovanje i Glavna nagrada dobitnik može da preuzme na dan i u vreme koje će biti dogovoreno sa Priređivačem u Ušće Shopping Centru, u roku od 7 (sedam) dana od dana izvlačenja nagrade. </li>
          <li class="text-white">Prilikom svakog preuzimanja nagrade, dobitnici moraju dokazati identitet uvidom u ličnu kartu i pokazati originalni račun, odnosno račune sa kojima su učestvovali i osvojili nagradu uz dokaz da takvom licu pripada Registracioni broj koji je osvojio nagradu i moraju potpisati izjavu o preuzimanju nagrade. Podaci iz lične karte i računa i Registracioni broj moraju da odgovaraju podacima koji su kao takvi registrovani na internet stranici i koji su osvojili konkretnu nagradu.</li>
          <li class="text-white">Pored navedenog, dobitnici Poklon vaučera za robu i usluge-kartice za dobitak od 111.000,00 dinara, Poklon vaučera za Nagradno putovanje i Glavne nagrade će pre preuzimanja nagrade Priređivaču morati i da dostave svoj jedinstveni matični broj (JMBG) i adresu i opštinu prebivališta, a sve kako bi Priređivač u skladu sa Zakonom o porezu na dohodak građana izvršio prijavu poreza na dohodak građana i izvršio plaćanje navedenog poreza..</li>
          <li class="text-white">Ukoliko je lice sa ograničenom poslovnom sposobnošću dobitnik bilo koje od nagrada koje čine Nagradni fond, preuzimanje nagrade vrše preko svojih zakonskih zastupnika.</li>
          <li class="text-white">Ukoliko dobitnici ne preuzmu nagrade u rokovima i na način kako je to ovim Pravilima regulisano, Priređivač nije u obavezi da izvučenom dobitniku uruči nagradu i može njome slobodno raspolagati. </li>
          <li class="text-white">Osim obaveze plaćanja navedenog poreza, Priređivač neće imati bilo kakvih drugih obaveza prema dobitnicima ovih nagrada. Ukoliko dobitnici ovih nagrada odbiju da Priređivaču daju podatke koji su mu neophodni radi plaćanja poreza na dohodak građana, dobitnici će biti obveznici plaćanja ovog poreza i samostalno će snositi ovaj porez.</li>
          <li class="text-white">Sve obaveze Priređivača prema dobitniku nagrade prestaju u momentu preuzimanja nagrade od strane dobitnika nagrade ili lica koje on za to ovlasti, odnosno protekom roka za preuzimanje nagrada.</li>
          <li class="text-white">Priređivač zadržava pravo da diskvalifikuje i onemogući učestvovanje u nagradnoj igri, odnosno da uskrati dodelu nagrada, onim licima koja ne poštuju Pravila nagradne igre, narušavaju javni red i mir, prouzrokuju štetu Priređivaču, Ušće Shopping Centru, posetiocima ili zakupcima Ušće Shopping Centra, koji se ponašaju prevarno ili ne poštuju pozitivne propise Republike Srbije.</li>
          <li class="text-white">Nadzor nad sprovođenjem nagradne igre vršiće Komisija za nadzor, imenovana od strane Priređivača, koju čine tri člana Uprave Ušće Shopping Centra.</li>
          <li class="text-white">me i prezime dobitnika Poklon vaučer za robu i usluge-kartice za dobitak od 111.000,00 dinara, Poklon vaučera za Nagradno putovanje i Glavne nagrade, kao i lista preostalih dodeljenih nagrada, biće objavljeni u dnevnim novinama koje se distribuiraju na celoj teritoriji Republike Srbije, najkasnije 15 dana nakon završetka nagradne igre, na internet stranici Ušće Shopping Centra- <a href="www.usceshoppingcenter.com" target="_blank">www.usceshoppingcenter.com </a> i društvenim mrežama Ušće Shopping Centra.</li>
        </ul>
        <ul class="rules-list">
          <li class="text-white text-center">Član 9.</li>
          <li class="text-white text-center">Porez</li>
        </ul>
        <ul class="rules-list">
          <li class="text-white">Priređivač preuzima obavezu plaćanja poreza na dohodak građana za dobitnike Poklon vaučera za robu i usluge-kartice za dobitak od 111.000,00 dinara, Poklon vaučera za Nagradno putovanje i Glavne nagrade, kojima je nagrada uručena.</li>
          <li class="text-white">Po završetku nagradne igre, Priređivač će u roku od 15 dana obavestiti nadležnu Ministarstvo finansija, Upravu za igre na sreću o rezultatima igre, odnosno dostaviti izveštaj o utvrđenim rezultatima nagradne igre.</li>
        </ul>
        <ul class="rules-list">
          <li class="text-white text-center">Član 10.</li>
          <li class="text-white text-center">Viša sila</li>
        </ul>
        <ul class="rules-list">
          <li class="text-white">Nagradna igra se može prekinuti u slučaju delovanja više sile i/ili nastupanja slučaja koje Priređivač ne može otkloniti. U slučaju prekida nagradne igre, javnost će biti obaveštena putem dnevnog lista, koji se distribuira na celoj teritoriji Republike Srbije.</li>
        </ul>
        <ul class="rules-list">
          <li class="text-white text-center">Član 11.</li>
          <li class="text-white text-center">Završne odredbe</li>
        </ul>
        <ul class="rules-list">
          <li class="text-white">Nakon stupanja na snagu Pravila nagradne igre će biti objavljena u dnevnom listu Blic, najkasnije 15 dana pre početka nagradne igre, na internet stranici Ušće Shopping Centra, kao i u samom Ušće Shopping Centru na mestu organizovanja nagradne igre.</li>
          <li class="text-white">U slučaju spora, utvrđuje se nadležnost suda u Beogradu.</li>
          <li class="text-white">Pravila nagradne igre stupaju na snagu po dobijanju saglasnosti Ministarstva finansija Republike Srbije, Uprave za igre na sreću.</li>
          <li class="text-white">U Beogradu, 7. februara 2020. godine</li>
        </ul>
      </div>
    </div>
    <div class="pravila-right">
      <img src="{{asset('/img/pravila-desk-background.png')}}" alt="">
    </div>
    <div class="pravila-fixed-logo">
      <img src="{{asset('/img/footer_logo.png')}}" alt="">
    </div>
  </div>
  </div>
@endsection
