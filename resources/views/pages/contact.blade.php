@extends('layouts.app')

@section('content')

  <div class="container contact-parent-container">
    <div class="desktop contact-desktop">

      <div class="row">
        <div class="container contact-child-container">
          <div class="contact-background">
            <img src="{{asset('/img/contact-mob-cover.png')}}" alt="">
          </div>
          <div class="contact-container">
            <div class="container_section flex_row">
              <figure class="mr-3">
                <img src="{{ asset('/img/kontakt_thumb.png') }}" alt="" class="img-fluid">
              </figure>
              <h1 class="text-white text-uppercase">Kontakt</h1>
            </div>
            <div class="container_section">
              <h4 class="text-white text-center"><b>Ušće Shopping Center info desk:</b></h4>
              <p class="text-white">Tel: +381 11 258 45 05</p>
            </div>
            <div class="container_section">
              <h3 class="text-white"><b>Ušće call centar</b></h3>
              <h4 class="text-white"><b>za primedbe i komentare:</b></h4>
            </div>
            <div class="container_section">
              <p class="text-white">Tel: +381 11 44 29 140</p>
              <p class="text-white">Fax: +381 11 260 14 13</p>
            </div>
            <div class="container_section special-container">
              <h5 class="text-white"><b>Radno vreme:</b></h5>
              <p class="text-white">&nbsp; 10h - 22h</p>
            </div>
            <div class="container_section special-container">
              <h5 class="text-white"><b>Email</b>: </h5>
              <p class="text-white">&nbsp; info@usceshoppingcenter.com</p>
            </div>
          </div>
          <div class="bottom-logo"></div>
        </div>
      </div>
    </div>

    <div class="mobile">

      <div class="row">
        <div class="container contact-child-container">
          <div class="contact-background">
            <img src="{{asset('/img/contact-mob-cover.png')}}" alt="">
          </div>
          <div class="contact-container">
            <div class="container_section flex_row">
              <figure class="mr-3">
                <img src="{{ asset('/img/kontakt_thumb.png') }}" alt="" class="img-fluid">
              </figure>
              <h1 class="text-white text-uppercase">Kontakt</h1>
            </div>
            <div class="container_section">
              <h4 class="text-white text-center"><b>Ušće Shopping Center info desk:</b></h4>
              <p class="text-white">Tel: +381 11 258 45 05</p>
            </div>
            <div class="container_section">
              <h3 class="text-white"><b>Ušće call centar</b></h3>
              <h4 class="text-white"><b>za primedbe i komentare:</b></h4>
            </div>
            <div class="container_section">
              <p class="text-white">Tel: +381 11 44 29 140</p>
              <p class="text-white">Fax: +381 11 260 14 13</p>
            </div>
            <div class="container_section special-container">
              <h5 class="text-white"><b>Radno vreme:</b></h5>
              <p class="text-white">&nbsp; 10h - 22h</p>
            </div>
            <div class="container_section special-container">
              <h5 class="text-white"><b>Email</b>: </h5>
              <p class="text-white">&nbsp; info@usceshoppingcenter.com</p>
            </div>
          </div>
          <div class="bottom-logo"></div>
        </div>
      </div>
    </div>
  </div>

@endsection
