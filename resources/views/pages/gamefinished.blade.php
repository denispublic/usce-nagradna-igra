@extends('layouts.app')

@section('content')

<div class="headline">
    <div class="headline_title">
      <h1 class="text-uppercase text-center text-white">Velika Ušće nagradna igra</h1>
      <img src="{{ asset('/img/osecaj.png') }}" alt="osecaj" class="img-fluid">
    </div>
  </div>

<div class="container afterhours_image__container game-over">
  <figure>
    <img src="{{ asset('/img/10_god_left.png') }}" alt="10_god_left" class="img-fluid">
  </figure>
  <div class="row">
    <div class="afterhours_header__content">
      <p class="text-left mozes"><b>Hvala na interesovanju!</b></p>
      <p class="text-uppercase mozes">Velika ušće nagradna igra <span class="text-lowercase">je <b>završena 30.10.2019.</b></span></p>
    </div>
  </div>
</div>

@endsection
