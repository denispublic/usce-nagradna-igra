@extends('layouts.app')

@section('content')
<style>
  @media(max-width: 1140px) {
    #modal1 {
      width: 90% !important;
    }
    #modal1 > .modal-content > h1 {
      font-size: 40px !important;
    }
  }
  @media(max-width: 767px) {
    #modal1 > .modal-content {
      padding: 130px 100px 130px 100px !important;
    }
    #modal1 > .modal-content > h1 {
      font-size: 1.7rem !important;
    }
  }
  @media(max-width: 500px) {
    #modal1 > .modal-content {
        padding: 90px 50px 90px 50px !important;
    }
  }
</style>
<div id="modal1" class="modal w-45" style="position:absolute;top:17%;width: 1024px;">
  <div class="modal-content" style="background:#ed1c24 !important;padding: 170px 100px 170px 100px;">
    <h1 style="color:#fff;font-weight:bold;text-align:center;font-size:50px;">
      U NAGRADNOJ IGRI MOŽEŠ UČESTVOVATI OD 10 DO 22H SVAKOG DANA
    </h1>
    <figure class="mb-0 karta modal_footer__img">
      <img src="{{ asset('/img/footer_logo.png') }}" alt="" class="img-fluid">
    </figure>
  </div>
  <div class="modal-close">
    <div class="icon-bar-1"></div>
    <div class="icon-bar-2"></div>
  </div>
</div>

<div class="headline">
    <div class="headline_title">
      <h1 class="text-uppercase text-center text-white">Velika Ušće nagradna igra</h1>
      <img src="{{ asset('/img/osecaj.png') }}" alt="osecaj" class="img-fluid">
    </div>
  </div>

<div class="container afterhours_image__container">
  <figure>
    <img src="{{ asset('/img/10_god_left.png') }}" alt="10_god_left" class="img-fluid">
  </figure>
  <div class="row">
    <div class="afterhours_header__content">
      <p class="text-uppercase mozes">U nagradnoj igri možeš učestvovati <b>od 10 do 22h svakog dana</b></p>
      <p class="text-white text-uppercase text_big">Vidimo se u 10h!</p>
    </div>
  </div>
</div>

@endsection
@section('javascript')
  <script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
  <script>
  $(document).ready(function() {
    setTimeout(function(){
      $('#modal1').css('display', 'block');
    }, 750);
    $(window).click(function() {
    if($('#modal1')) {
      $('#modal1').css('display', 'none');
      }
    })
  })
  </script>

@endsection
