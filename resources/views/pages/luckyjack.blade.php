@extends('layouts.app')

@section('content')
<div id="looser-modal" class="container">
  <div class="looser-desk-back">
    <img src="{{ asset('/img/zao-nam-je-desk-back.png') }}" alt="">
  </div>
  <div class="kombinacija-brojeva-container">
    <h4 class="kombinacija-brojeva-title">Vaša kombinacija brojeva:</h4>
    <div class="kombinacija-brojeva"></div>
  </div>
</div>
<div class="container luckyjack-container">

  <div class="row slot-row">
    <div class="lucky-jack-background">
      <img src="{{ asset('/img/lucky-jack-background.png') }}" alt="" class="lucky-jack-desk-backg">
      <img src="{{ asset('/img/lucky-jack-mob-background.png') }}" alt="" class="lucky-jack-mob-backg">
    </div>
    <div class="container">
      <div class="slot-machine three-slot-machine"></div>
      <button type="button" id="spin-button" class="submit_btn">igraj</button>

      <div id="modal1" class="modal w-45">
        <div class="modal-content">
          <h2 class="text-white text-uppercase modal_title">Čestitamo!</h2>

          <div class="row modal-sub-img">
            <div class="mb-3 modal_subtitle__container">
              <div class="modal_subtitle"><h4><strong>Osvojili ste</strong></h4><p><strong>Ušće Poklon karticu</strong></p><p><strong>u vrednosti <span id="reward_value"></span></strong></p></div>
            </div>
            <div class="text-center">
              <figure class="mb-0">
                <img src="" alt="" id="reward_img" class="img-fluid">
              </figure>
            </div>
          </div>
          <figure class="mb-0 karta modal_footer__img">
            <img src="{{ asset('/img/footer_logo.png') }}" alt="" class="img-fluid">
          </figure>
        </div>
        <a href="/" class="modal-close">
          <div class="icon-bar-1"></div>
          <div class="icon-bar-2"></div>
        </a>
      </div>

      <div id="modal2" class="modal">
        <div class="modal-content">

          <h2 class="text-white text-uppercase modal_title">Žao nam je,</h2>
          <p class="small text-uppercase">niste osvojili instant nagradu,</p>
          <p class="text-white text-uppercase modal_title2">Ali igra se nastavlja!</p>
          <div class="mb-5 text-center">
            <span class="modal_subtitle">Svi registrovani korisnicni učestvuju u izvlačenju glavne nagrade.</span>
          </div>
          <div class="row top_row">
            <div class="col-lg-5 col-12">
              <figure class="mb-0">
                <img src="{{ asset('/img/modal2.png') }}" alt="modal2" class="img-fluid">
              </figure>
            </div>
            <div class="col-lg-7 col-12">
              <span class="text-white text-center text-uppercase glavna">Glavna nagrada</span>
              <p class="text-white"><b>Desetodnevno all inclusive putovanje na ŠRI LANKU</b>, za dve osobe. <b>Izvlačenje 30.10 u 18:00h</b></p>
            </div>
          </div>
          <figure class="mb-0 modal_footer__img">
            <img src="{{ asset('/img/footer_logo.png') }}" alt="" class="img-fluid">
          </figure>
        </div>
        <a href="/" class="modal-close">
          <div class="icon-bar-1"></div>
          <div class="icon-bar-2"></div>
        </a>
      </div>
    </div>
  </div>
</div>
<div class="lucky-jack-nagrade col-lg-6 col-md-12">
  <div class="nagrade-inner-left"></div>
  <div class="nagrade-inner-right">
    <h5>Dnevne nagrade</h5>
    <p>Osvojite <strong>Vaučer za kupovinu</strong> u UŠĆU, svakog dana u periodu od 16. marta do 12. aprila, ili do podele svih nagrada.</p>
    <p><strong>111 x 3.000 RSD i 111 x 6.000 RSD</strong></p>
  </div>
</div>
<div class="register-bottom-logo">
  <img src="{{asset('/img/footer_logo.png')}}" alt="">
</div>

  @section('javascript')
  <script src="{{ asset('/js/plugins/TweenMax/TweenMax.min.js') }}"></script>
  <script src="{{ asset('/js/one-slot.js') }}"></script>
  <script src="{{ asset('/js/slot.js') }}"></script>
  <script>

    var lapTopScreen = window.matchMedia('(min-width:601px) and (max-width: 1200px)');
    var phoneScreen = window.matchMedia('(max-width: 600px)');
      if(lapTopScreen.matches) {
        var initObj = {
          image_width: 100,
          image_height: 75,
          padding: 0,
          number_of_slots: 3,
          images: [
            '{{ asset("/img/3000.png") }}',
            '{{ asset("/img/6000.png") }}',
            {{--'{{ asset("/img/12000.png") }}'--}}
          ]
        };
      } else if(phoneScreen.matches) {
        var initObj = {
          // image_width: 171,
          image_width: 80,
          image_height: 58,
          padding: 0,
          number_of_slots: 3,
          images: [
            '{{ asset("/img/3000.png") }}',
            '{{ asset("/img/6000.png") }}',
            {{--'{{ asset("/img/12000.png") }}'--}}
          ]
        };
      } else {
        var initObj = {
          // image_width: 171,
          image_width: 143,
          image_height: 93,
          padding: 0,
          number_of_slots: 3,
          images: [
            '{{ asset("/img/3000.png") }}',
            '{{ asset("/img/6000.png") }}',
            {{--'{{ asset("/img/12000.png") }}'--}}
          ]
        };
      }


      var images = [
        '{{ asset("/img/popup-kartica-3000.png") }}',
        '{{ asset("/img/popup-kartica-6000.png") }}',
        {{--'{{ asset("/img/12000-kartica.png") }}',--}}
      ];

      var machine = new SlotMachine(initObj, $(".slot-machine"));

      $(document).ready(function() {
          $.get("/dailywinnerjson").done(function(data) {
              var responseData = data;
              console.log(responseData)
              var responseDataArray = (""+responseData).split("");
              console.log(responseDataArray);
              var amount = responseData.ammount;
              if(amount) {
                var amountArray = Array.from(amount);
                if (amount === '3000') {
                  amountArray.splice(1, 0, '.');
                }
                if(amount === '6000') {
                  amountArray.splice(1, 0, '.');
                }
                var string = amountArray.join('');
              }


              $("#spin-button").click(function() {
                this.style.pointerEvents = 'none';

                machine.spin(amount, function(amount) {
                  if(amount) {
                    var modal1 = document.querySelector("#modal1");
                    var instance1 = M.Modal.init(modal1);
                    instance1.open();

                    document.getElementById("reward_value").innerText = string + ' rsd';
                    var re = new RegExp(amount, 'g');
                    console.log(re);
                    if(re == "/3000/g") {
                      document.getElementById("reward_img").src = images[0];
                    } else if (re == "/6000/g") {
                      document.getElementById("reward_img").src = images[1];
                    }
                  } else {
                    var modal2 = document.querySelector("#modal2");
                    var looserModal = document.querySelector('#looser-modal');
                    var initialContent = document.querySelector('.luckyjack-container');
                    var initialContentBottom = document.querySelector('.lucky-jack-nagrade');
                    //change bellow with data from json

                    //change above with data from json
                    initialContent.style.display = "none";
                    initialContentBottom.style.display = "none";
                    looserModal.style.display = "block";
                    var codeGeneratedDiv = document.querySelector('.kombinacija-brojeva');
                    var generisanId = '';
                    responseDataArray.map(function(item){
                      generisanId = '<span class="numberBox"><p>' + item + '<p><span>';
                      codeGeneratedDiv.innerHTML += generisanId;
                    });

                    // var instance2 = M.Modal.init(modal2);
                    //  var instance2 = M.Modal.init(looserModal);
                    // instance2.open();
                  }
                });
              });
          }).fail(function(data) {
            console.log(data);
          });
      });

      document.addEventListener("click", function(e){
        if (document.querySelector('#modal1').style.display === 'block' || document.querySelector('#modal2').style.display === 'block'
        && e.target !== document.querySelector('#modal1 .modal-content') && e.target !== document.querySelector('#modal2 .modal-content')) {
          window.location.replace('/');
        }
      });
  </script>
  @endsection

@endsection
