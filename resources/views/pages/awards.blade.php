@extends('layouts.app')

@section('content')

<div class="container awards-container">

  <div class="desktop">
    <div class="row main-row">
        <img src="{{ asset('/img/HomePageBackground.png') }}" alt="">
    </div>
      <div class="mehanizam">
          <div class="mehanizam-left col-lg-6">
              <div class="mehanizam-inner-left"></div>
              <div class="mehanizam-inner-right">
                  <h5>8 Vaučera po 111.000 rsd</h5>
                  <p><strong>Vaučeri</strong> za kupovinu u UŠĆU u vrednosti od <strong>111.000 RSD.</strong></p>
                  <p>Izvlačenje <strong>18, 22, 25, 28. marta</strong> i <strong>1, 5, 8, 12. aprila</strong> u <strong>18h.</strong></p>
              </div>
          </div>
          <div class="mehanizam-right  col-lg-6">
              <div class="mehanizam-inner-left"></div>
              <div class="mehanizam-inner-right">
                  <h5>Dnevne nagrade</h5>
                  <p>Osvojite <strong>Vaučer za kupovinu</strong> u UŠĆU, svakog dana u periodu od 16. marta do 12. aprila, ili do podele svih nagrada.</p>
                  <p><strong>111 x 3.000 RSD i 111 x 6.000 RSD</strong></p>
              </div>
          </div>
      </div>
      <div class="bottom-logo"></div>
  </div>

  <div class="mobile">
    <div class="row main-row">
      <img src="{{ asset('/img/home-mob-back.png') }}" alt="">
    </div>
    <div class="mehanizam">
      <div class="mehanizam-left col-lg-6">
        <div class="mehanizam-inner-left"></div>
        <div class="mehanizam-inner-right">
          <h5>8 Vaučera po 111.000 rsd</h5>
          <p><strong>Vaučeri</strong> za kupovinu u UŠĆU u vrednosti od <strong>111.000 RSD.</strong></p>
          <p>Izvlačenje <strong>18, 22, 25, 28. marta</strong> i <strong>1, 5, 8, 12. aprila</strong> u <strong>18h.</strong></p>
        </div>
      </div>
      <div class="mehanizam-right  col-lg-6">
        <div class="mehanizam-inner-left"></div>
        <div class="mehanizam-inner-right">
          <h5>Dnevne nagrade</h5>
          <p>Osvojite <strong>Vaučer za kupovinu</strong> u UŠĆU, svakog dana u periodu od 16. marta do 12. aprila, ili do podele svih nagrada.</p>
          <p><strong>111 x 3.000 RSD i 111 x 6.000 RSD</strong></p>
        </div>
      </div>
    </div>
    <div class="bottom-logo"></div>
</div>


@endsection
