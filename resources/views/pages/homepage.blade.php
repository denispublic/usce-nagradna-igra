@extends('layouts.app')

@section('content')
<div class="container">


  <div class="desktop">
    <div class="row main-row">
        <img src="{{ asset('/img/HomePageBackground.png') }}" alt="">
    </div>

      <div class="mehanizam">
        <div class="mehanizam-left col-lg-6">
            <div class="mehanizam-inner-left"></div>
            <div class="mehanizam-inner-right">
                <h5>8 Vaučera po 111.000 rsd</h5>
                <p><strong>Vaučeri</strong> za kupovinu u UŠĆU u vrednosti od <strong>111.000 RSD.</strong></p>
                <p>Izvlačenje <strong>18, 22, 25, 28. marta</strong> i <strong>1, 5, 8, 12. aprila</strong> u <strong>18h.</strong></p>
            </div>
        </div>
        <div class="mehanizam-right  col-lg-6">
            <div class="mehanizam-inner-left"></div>
            <div class="mehanizam-inner-right">
                <h5>Dnevne nagrade</h5>
                <p>Osvojite <strong>Vaučer za kupovinu</strong> u UŠĆU, svakog dana u periodu od 16. marta do 12. aprila, ili do podele svih nagrada.</p>
                <p><strong>111 x 3.000 RSD i 111 x 6.000 RSD</strong></p>
            </div>
        </div>
    </div>

    <h2 class="homepage_title">Dobrodošli! Registruj se i osvoji:</h2>
    <a class="go-to-prijava" href="{{ route('events') }}">Registracija</a>
      <div id="modal-terms" class="modal-terms d-none">
          <p>Morate prihvatiti pravila igre!</p>

          <a class="modal-close">
              <div class="icon-bar-1"></div>
              <div class="icon-bar-2"></div>
          </a>
      </div>
    
{{--    <a class="go-to-prijava" href="{{ route('events') }}">Registracija</a>--}}
{{--   <script>--}}
{{--     document.querySelector('.homepage_register__form .submit_btn').addEventListener('click', function(e) {--}}
{{--       if (document.querySelector('.accept_terms input').checked == false) {--}}
{{--         e.preventDefault();--}}
{{--         document.querySelector('.modal-terms').classList.remove('d-none');--}}
{{--       }--}}
{{--     });--}}
{{--     document.querySelector('.modal-terms .modal-close').addEventListener('click', function() {--}}
{{--       document.querySelector('.modal-terms').classList.add('d-none');--}}
{{--     });--}}
{{--   </script>--}}
    

  </div>

  <div class="mobile">
      <img src="{{ asset('/img/home-mob-back.png') }}" alt="mob-cover" class="mob-home-back">
      <div class="mehanizam">
          <div class="mehanizam-left col-lg-6">
              <div class="mehanizam-inner-left"></div>
              <div class="mehanizam-inner-right">
                  <h5>8 Vaučera po 111.000 rsd</h5>
                  <p><strong>Vaučeri</strong> za kupovinu u UŠĆU u vrednosti od <strong>111.000 RSD.</strong></p>
                  <p>Izvlačenje <strong>18, 22, 25, 28. marta</strong> i <strong>1, 5, 8, 12. aprila</strong> u <strong>18h.</strong></p>
              </div>
          </div>
          <div class="mehanizam-right">
              <div class="mehanizam-inner-left"></div>
              <div class="mehanizam-inner-right">
                  <h5>Dnevne nagrade</h5>
                  <p>Osvojite <strong>Vaučer za kupovinu</strong> u UŠĆU, svakog dana u periodu od 16. marta do 12. aprila, ili do podele svih nagrada.</p>
                  <p><strong>111 x 3.000 RSD i 111 x 6.000 RSD</strong></p>
              </div>
          </div>
      </div>
      <h1 class="homepage_title">Dobrodošli! Registruj se i osvoji:</h1>
    <a class="go-to-prijava" href="{{ route('events') }}">Registracija</a>
{{--      {!! Form::open(['action' => 'HomepageController@terms', 'method' => 'POST', 'class' => 'homepage_register__form container mt-5']) !!}--}}
{{--    <div class="home-footer-background">--}}
{{--      <img src="{{ asset('/img/homeFooterBackground.png') }}" alt="">--}}
{{--    </div>--}}
{{--      <div class="form-group">--}}
{{--          {{ Form::submit('Registruj se', ['class' => 'submit_btn']) }}--}}
{{--      </div>--}}
{{--      <div class="form-group accept_terms">--}}
{{--          {{ Form::label('terms-and-conditions', 'Prihvatam pravila nagradne igre') }}--}}
{{--          {{ Form::checkbox('terms-and-conditions', true, false, ['id' => 'terms-and-conditions']) }}--}}
{{--      </div>--}}
{{--      {!! Form::close() !!}--}}

      <div id="modal-terms" class="modal-terms d-none">
          <p>Morate prihvatiti pravila igre!</p>

          <a class="modal-close">
              <div class="icon-bar-1"></div>
              <div class="icon-bar-2"></div>
          </a>
      </div>
  </div>
</div>
<div class="register-bottom-logo">
  <img src="{{asset('/img/footer_logo.png')}}" alt="">
</div>
@endsection
