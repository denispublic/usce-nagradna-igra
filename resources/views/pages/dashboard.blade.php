@extends('layouts.app')

@section('content')
<div class="dashboard-container">
    <div class="dashboard_head__container">
        <span class="text-white dashboard_title text-uppercase bg_green">Kontrolna tabla</span>
        <div class="dashboard_controls">
            {!! Form::open(['method' => 'POST', 'action' => 'LogoutController@logout']) !!}
                {{ Form::submit('Logout', ['class' => 'bg_red submit_btn']) }}
            {!! Form::close() !!}
        </div>
    </div>

    <ul class="nav nav-pills" id="dashboard-pills" role="tablist">
        <li class="nav-item">
            <a class="nav-link bg_red active" id="pills-daily-tab" data-toggle="pill" href="#pills-daily" role="tab" aria-controls="pills-daily" aria-selected="true">Dnevni dobitnici</a>
        </li>
        <!--<li class="nav-item">
            <a class="nav-link bg_green" id="pills-tendaily-tab" data-toggle="pill" href="#pills-tendaily" role="tab" aria-controls="pills-tendaily" aria-selected="false">Desetodnevni dobitnici</a>
        </li>-->
      <li class="nav-item">
        <a class="nav-link bg_orange" id="pills-travel-tab" data-toggle="pill" href="#pills-travel" role="tab" aria-controls="pills-travel" aria-selected="false">Dobitnici putovanja</a>
      </li>
        <li class="nav-item">
            <a class="nav-link bg_orange" id="pills-main-tab" data-toggle="pill" href="#pills-main" role="tab" aria-controls="pills-main" aria-selected="false">Glavni dobitnik</a>
        </li>
        <li class="nav-item">
            <a class="nav-link bg_red" id="pills-all-tab" data-toggle="pill" href="#pills-all" role="tab" aria-controls="pills-all" aria-selected="true">Dobitnici vaučera od 111.000RSD</a>
        </li>
    </ul>

    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade" id="pills-travel" role="tabpanel" aria-labelledby="pills-travel-tab">
        <table id="winners5" class="table" cellspacing="0" width="100%">
          <thead>
          <tr>
            <th class="th-sm">Korisnik</th>
            <th class="th-sm">E-mail adresa</th>
            <th class="th-sm">Broj telefona</th>
            <th class="th-sm">Broj računa 1</th>
            <th class="th-sm">Iznos računa 1</th>
            <th class="th-sm">Broj računa 2</th>
            <th class="th-sm">Iznos računa 2</th>
            <th class="th-sm">Datum upisa</th>
            <th class="th-sm">ID Broj</th>
            <th class="th-sm">Nagrada</th>
          </tr>
          </thead>
          <tbody>
          @foreach($users as $user)
            @if($user->prize_type == 'Madrid putovanje')
              <tr>
                <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->phone_number }}</td>
                <td>{{ $user->bill_number1 }}</td>
                <td>{{ number_format($user->bill_ammount1, '2', ',', '.') }} RSD</td>
                <td>{{ ($user->bill_number2) ? $user->bill_number2 : '/' }}</td>
                <td>{{ ($user->bill_ammount2) ? $user->bill_ammount2 . ' RSD' : '/' }}</td>
                <td>{{ $user->created_at }}</td>
                <td>{{ $user->generated_id }}</td>
                <td>Putovanje u Madrid</td>
              </tr>
            @endif
          @endforeach
          @foreach($users as $user)
            @if($user->prize_type == 'London putovanje')
              <tr>
                <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->phone_number }}</td>
                <td>{{ $user->bill_number1 }}</td>
                <td>{{ number_format($user->bill_ammount1, '2', ',', '.') }} RSD</td>
                <td>{{ ($user->bill_number2) ? $user->bill_number2 : '/' }}</td>
                <td>{{ ($user->bill_ammount2) ? $user->bill_ammount2 . ' RSD' : '/' }}</td>
                <td>{{ $user->created_at }}</td>
                <td>{{ $user->generated_id }}</td>
                <td>Putovanje u London</td>
              </tr>
            @endif
          @endforeach
          </tbody>
        </table>
      </div>
        <div class="tab-pane show active" id="pills-daily" role="tabpanel" aria-labelledby="pills-daily-tab">
            <table id="winners2" class="table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="th-sm">Korisnik</th>
                        <th class="th-sm">E-mail adresa</th>
                        <th class="th-sm">Broj telefona</th>
                        <th class="th-sm">Broj računa 1</th>
                        <th class="th-sm">Iznos računa 1</th>
                        <th class="th-sm">Broj računa 2</th>
                        <th class="th-sm">Iznos računa 2</th>
                        <th class="th-sm">Datum upisa</th>
                        <th class="th-sm">ID Broj</th>
                        <th class="th-sm">Nagrada</th>
                        <th class="th-sm">Tip nagrade</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    @if($user->prize_type == 'Dnevni dobitnik')
                        <tr>
                            <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->phone_number }}</td>
                            <td>{{ $user->bill_number1 }}</td>
                            <td>{{ number_format($user->bill_ammount1, '2', ',', '.') }} RSD</td>
                            <td>{{ ($user->bill_number2) ? $user->bill_number2 : '/' }}</td>
                            <td>{{ ($user->bill_ammount2) ? $user->bill_ammount2 . ' RSD' : '/' }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>{{ $user->generated_id }}</td>
                            <td>{{ number_format($user->ammount, '2', ',', '.') }} RSD</td>
                            <td>Dnevni dobitnik</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>

{{--        MENJAJ PRIZE TYPE U FOREACHU   DA BI PRIKAZAO OSTALE DOBITNIKE    --}}

        <div class="tab-pane fade" id="pills-tendaily" role="tabpanel" aria-labelledby="pills-tendaily-tab">
            <table id="winners3" class="table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="th-sm">Korisnik</th>
                        <th class="th-sm">E-mail adresa</th>
                        <th class="th-sm">Broj telefona</th>
                        <th class="th-sm">Broj računa 1</th>
                        <th class="th-sm">Iznos računa 1</th>
                        <th class="th-sm">Broj računa 2</th>
                        <th class="th-sm">Iznos računa 2</th>
                        <th class="th-sm">Datum upisa</th>
                        <th class="th-sm">ID Broj</th>
                        <th class="th-sm">Nagrada</th>
                        <th class="th-sm">Tip nagrade</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        @if($user->prize_type == '10 day winner')
                            <tr>
                                <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone_number }}</td>
                                <td>{{ $user->bill_number1 }}</td>
                                <td>{{ number_format($user->bill_ammount1, '2', ',', '.') }} RSD</td>
                                <td>{{ ($user->bill_number2) ? $user->bill_number2 : '/' }}</td>
                                <td>{{ ($user->bill_ammount2) ? $user->bill_ammount2 . ' RSD' : '/' }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>{{ $user->generated_id }}</td>
                                <td>{{ number_format($user->ammount, '2', ',', '.') }} RSD</td>
                                <td>Desetodnevni dobitnik</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="pills-main" role="tabpanel" aria-labelledby="pills-main-tab">
            <table id="winners4" class="table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="th-sm">Korisnik</th>
                        <th class="th-sm">E-mail adresa</th>
                        <th class="th-sm">Broj telefona</th>
                        <th class="th-sm">Broj računa 1</th>
                        <th class="th-sm">Iznos računa 1</th>
                        <th class="th-sm">Broj računa 2</th>
                        <th class="th-sm">Iznos računa 2</th>
                        <th class="th-sm">Datum upisa</th>
                        <th class="th-sm">ID Broj</th>
                        <th class="th-sm">Nagrada</th>
                        <th class="th-sm">Tip nagrade</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        @if($user->prize_type == 'Main winner')
                            <tr>
                                <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone_number }}</td>
                                <td>{{ $user->bill_number1 }}</td>
                                <td>{{ number_format($user->bill_ammount1, '2', ',', '.') }} RSD</td>
                                <td>{{ ($user->bill_number2) ? $user->bill_number2 : '/' }}</td>
                                <td>{{ ($user->bill_ammount2) ? $user->bill_ammount2 . ' RSD' : '/' }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>{{ $user->generated_id }}</td>
                                <td>{{ number_format($user->ammount, '2', ',', '.') }} &euro;</td>
                                <td>Glavni dobitnik</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
            <table id="winners" class="table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="th-sm">Korisnik</th>
                        <th class="th-sm">E-mail adresa</th>
                        <th class="th-sm">Broj telefona</th>
                        <th class="th-sm">Broj računa 1</th>
                        <th class="th-sm">Iznos računa 1</th>
                        <th class="th-sm">Broj računa 2</th>
                        <th class="th-sm">Iznos računa 2</th>
                        <th class="th-sm">Datum upisa</th>
                        <th class="th-sm">ID Broj</th>
                        <th class="th-sm">Nagrada</th>
                        <th class="th-sm">Tip nagrade</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    @if($user->winner === 0)
                        <tr>
                            <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->phone_number }}</td>
                            <td>{{ $user->bill_number1 }}</td>
                            <td>{{ number_format($user->bill_ammount1, '2', ',', '.') }} RSD</td>
                            <td>{{ ($user->bill_number2) ? $user->bill_number2 : '/' }}</td>
                            <td>{{ ($user->bill_ammount2) ? number_format($user->bill_ammount2, '2', ',', '.') . ' RSD' : '/' }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>{{ $user->generated_id }}</td>
                            <td>/</td>
                            <td>/</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{-- IZLVACENJE DOBITNIKA 111.000--}}
    <div class="dashboard_reward__controls row">
        {!! Form::open(['method' => 'GET', 'url' => '/izvlacenje-nedeljnog-dobitnika', 'class' => 'col-lg-6']) !!}
            {{ Form::submit('Izvlačenje dobitnika 111.000', ['class' => 'bg_orange']) }}
        {!! Form::close() !!}
        @if (session('error'))
        <span class="error-message">
            {{ session('error') }}
        </span>
        @endif
    </div>

    {{--IZVLACENJE DOBITNIKA MADRID--}}
    <div class="dashboard_reward__controls row">
        {!! Form::open(['method' => 'GET', 'url' => '/izvlacenje-putovanja-madrid', 'class' => 'col-lg-6']) !!}
            {{ Form::submit('Izvlačenje putavanja u Madrid', ['class' => 'bg_orange']) }}
        {!! Form::close() !!}
        @if (session('error'))
        <span class="error-message">
            {{ session('error') }}
        </span>
        @endif
    </div>

    {{--    IZVLACENJE DOBITNIKA LONDON--}}
    <div class="dashboard_reward__controls row">
        {!! Form::open(['method' => 'GET', 'url' => '/izvlacenje-putovanja-london', 'class' => 'col-lg-6']) !!}
            {{ Form::submit('Izvlačenje putavanja u London', ['class' => 'bg_orange']) }}
        {!! Form::close() !!}
        @if (session('error'))
        <span class="error-message">
            {{ session('error') }}
        </span>
        @endif
    </div>

    {{-- IZLVACENJE PUTOVANJA U LA--}}
    <div class="dashboard_reward__controls row">
        {!! Form::open(['method' => 'GET', 'url' => '/izvlacenje-putovanja-LA', 'class' => 'col-lg-6']) !!}
        {{ Form::submit('Izvlačenje putovanja u LA', ['class' => 'bg_orange']) }}
        {!! Form::close() !!}
        @if (session('error'))
            <span class="error-message">
            {{ session('error') }}
        </span>
        @endif
    </div>

</div>


    

    @section('javascript')
        <script>
            var $ = jQuery.noConflict();
            $(document).ready(function () {
                $('#winners, #winners2, #winners3, #winners4').DataTable({
                    "pagingType": "simple_numbers",
                    "lengthChange": false,
                    "info": false,
                    "bInfo" : false
                });
                $('.dataTables_length').addClass('bs-select');
            });
        </script>
    @endsection
@endsection
