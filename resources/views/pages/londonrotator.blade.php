@extends('layouts.app')

@section('content')

  <div id="winner-modal" class="container hidden-container">
    <div class="left">
      <img src="{{ asset('/img/london-winner-cestitamo-mob-back.png') }}" class="main-cestitamo-mob-cover" alt="">
      <img src="{{ asset('/img/madrid-winner-cestitamo-desk-left-top.png') }}" class="main-cestitamo-top-left"  alt="">
      <div class="kombinacija-brojeva-container">
        <h4 class="kombinacija-brojeva-title">Dobitnakombinacija brojeva:</h4>
        <div class="kombinacija-brojeva"></div>
      </div>
      <img src="{{ asset('/img/madrid-winner-cestitamo-desk-left-bottom.png') }}" class="main-cestitamo-bottom-left"  alt="">
    </div>
    <div class="right">
      <img src="{{ asset('/img/london-winner-cestitamo-desk-right.png') }}" alt="">
    </div>
  </div>

<div class="london-winner-rotator  main-winner-container container">
  <div class="row">
    <div class="container">
      <div class="top-background" style="margin-top: 0px;">
        <img src="{{asset('/img/london-winner-top-back.png')}}" alt="">
      </div>
      <div class="top-mob-background">
        <img src="{{asset('/img/london-winner-mob-top-back.png')}}" alt="">
      </div>
      <div class="slot-machine six-slot-machine"></div>
      <button class="submit_btn" id="start-rotate" type="button" name="button">Start</button>
      <div class="bottom-background">
        <img src="{{asset('/img/london-winner-bottom-back.png')}}" alt="">
      </div>
      <div class="bottom-mob-background">
        <img src="{{asset('/img/london-winner-mob-bottom-back.png')}}" alt="">
      </div>
    </div>
    <div class="register-bottom-logo main-win-bottom-logo">
      <img src="{{asset('/img/footer_logo.png')}}" alt="">
    </div>
  </div>
</div>


  @section('javascript')
  <script src="{{ asset('/js/plugins/TweenMax/TweenMax.min.js') }}"></script>
  <script src="{{ asset('/js/tdl_main_wnr.js') }}"></script>
  <script src="{{ asset('/js/rotator.js') }}"></script>
  <script>
    var lapTopScreen = window.matchMedia('(min-width:769px) and (max-width: 1200px)');
    var tabletScreen = window.matchMedia('(min-width:501px) and (max-width: 768px)');
    var phoneScreen = window.matchMedia('(max-width: 500px)');
    if(lapTopScreen.matches) {
      var initObj = {
        image_width: 60,
        image_height: 110,
        padding: 17,
        number_of_slots: 11,
        images: [
          '{{ asset("/img/0.png") }}',
          '{{ asset("/img/1.png") }}',
          '{{ asset("/img/2.png") }}',
          '{{ asset("/img/3.png") }}',
          '{{ asset("/img/4.png") }}',
          '{{ asset("/img/5.png") }}',
          '{{ asset("/img/6.png") }}',
          '{{ asset("/img/7.png") }}',
          '{{ asset("/img/8.png") }}',
          '{{ asset("/img/9.png") }}'
        ]
      };
    } else if (tabletScreen.matches) {
      var initObj = {
        image_width: 5,
        image_height: 80,
        padding: 17,
        number_of_slots: 11,
        images: [
          '{{ asset("/img/0.png") }}',
          '{{ asset("/img/1.png") }}',
          '{{ asset("/img/2.png") }}',
          '{{ asset("/img/3.png") }}',
          '{{ asset("/img/4.png") }}',
          '{{ asset("/img/5.png") }}',
          '{{ asset("/img/6.png") }}',
          '{{ asset("/img/7.png") }}',
          '{{ asset("/img/8.png") }}',
          '{{ asset("/img/9.png") }}'
        ]
      };
    } else if (phoneScreen.matches) {
      var initObj = {
        image_width: 5,
        image_height: 80,
        padding: 17,
        number_of_slots: 11,
        images: [
          '{{ asset("/img/0.png") }}',
          '{{ asset("/img/1.png") }}',
          '{{ asset("/img/2.png") }}',
          '{{ asset("/img/3.png") }}',
          '{{ asset("/img/4.png") }}',
          '{{ asset("/img/5.png") }}',
          '{{ asset("/img/6.png") }}',
          '{{ asset("/img/7.png") }}',
          '{{ asset("/img/8.png") }}',
          '{{ asset("/img/9.png") }}'
        ]
      };
    } else {
      var initObj = {
        image_width: 60,
        image_height: 110,
        padding: 17,
        number_of_slots: 11,
        images: [
          '{{ asset("/img/0.png") }}',
          '{{ asset("/img/1.png") }}',
          '{{ asset("/img/2.png") }}',
          '{{ asset("/img/3.png") }}',
          '{{ asset("/img/4.png") }}',
          '{{ asset("/img/5.png") }}',
          '{{ asset("/img/6.png") }}',
          '{{ asset("/img/7.png") }}',
          '{{ asset("/img/8.png") }}',
          '{{ asset("/img/9.png") }}'
        ]
      };
    }


    var machine = new SlotMachine(initObj, $(".slot-machine"));

    $(document).ready(function() {
      $.get("/londonwinnerjson").done(function(data) {
        var responseData = data,
          generatedID = responseData.generated_id,
          output = [];
        console.log(generatedID);
        console.log(responseData);

        var replacearr = [];

        for(var i=0; i<generatedID.length; i++) {
          replacearr.push(generatedID.charAt(i));
        }
        console.log(replacearr);

        document.querySelector('#start-rotate').addEventListener('click', function() {
          this.style.pointerEvents = 'none';
          machine.spin(replacearr, function() {
            var winnerModal = document.querySelector('#winner-modal');
            winnerModal.style.display = "flex";
            document.querySelector('.main-winner-container').style.display = "none";
            var codeGeneratedDiv = document.querySelector('.kombinacija-brojeva');
            var generisanId = '';
            replacearr.map(function(item){
              generisanId = '<span class="numberBox"><p>' + item + '<p><span>';
              codeGeneratedDiv.innerHTML += generisanId;
            });
          });
        });
      }).fail(function(data) {
        console.log(data);
      });
    });
  </script>
  @endsection
@endsection
