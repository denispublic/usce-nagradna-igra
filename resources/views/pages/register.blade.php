@extends('layouts.app')

@section('content')

  <div class="container register-container">
    <div class="register-content">
      <div class="register-background">
        <img src="{{asset('/img/register-background.png')}}" alt="" class="desktop-background">
        <img src="{{asset('/img/register-mob-background.png')}}" alt="" class="mobile-background">
      </div>
      <div class="input-form">
        {!! Form::open(['method' => 'POST', 'action' => 'RegisterController@register', 'class' => 'register_form']) !!}
        <div class="input-field">
          {{ Form::text('firstname', '', ['id' => 'firstname', 'class' => 'validate']) }}
          @if ($errors->has('firstname'))
            <span class="error-message">
              {{ $errors->first('firstname') }}
            </span>
          @endif
          {{ Form::label('firstname', 'Ime') }}
        </div>
        <div class="input-field">
          {{ Form::text('lastname', '', ['id' => 'lastname', 'class' => 'validate']) }}
          @if ($errors->has('lastname'))
            <span class="error-message">
              {{ $errors->first('lastname') }}
            </span>
          @endif
          {{ Form::label('lastname', 'Prezime') }}
        </div>
        <div class="input-field">
          {{ Form::text('phone', '', ['id' => 'phone', 'class' => 'validate']) }}
          <span class="input_placeholder">+381</span>
          @if ($errors->has('phone'))
            <span class="error-message">
              {{ $errors->first('phone') }}
            </span>
          @endif
          {{ Form::label('phone', 'Broj telefona') }}
        </div>
        <div class="input-field">
          {{ Form::text('email', '', ['id' => 'email', 'class' => 'validate']) }}
          @if ($errors->has('email'))
            <span class="error-message">
              {{ $errors->first('email') }}
            </span>
          @endif
          {{ Form::label('email', 'Email') }}
        </div>
        <div class="input-field">
          <div class="racuni-forma">
            <div class="racun1 special-input-borders">
              {{ Form::text('bill_number1', '', ['id' => 'bill_number1', 'class' => 'validate']) }}
              @if ($errors->has('bill_number1'))
                <span class="error-message">
                  {{ $errors->first('bill_number1') }}
                </span>
              @endif
              {{ Form::label('bill_number1', 'Broj računa') }}
            </div>

            <div class="racun2 special-input-borders">
            {{ Form::text('bill_number2', '', ['id' => 'bill_number2', 'class' => 'validate']) }}
            <!--@if ($errors->has('bill_number2'))
              <span class="error-message">
{{ $errors->first('bill_number2') }}
                </span>
@endif -->
              {{ Form::label('bill_number2', 'Broj računa 2') }}
            </div>
          </div>

        </div>
        <div class="input-field input-field-bill-amount">
          <div class="iznos-racuna-forma">
            <div class="iznos-racuna1">
              {{ Form::text('bill_ammount1', '', ['id' => 'bill_ammount1', 'class' => 'validate']) }}
              @if ($errors->has('bill_ammount1'))
                <span class="error-message">
                  {{ $errors->first('bill_ammount1') }}
                </span>
              @endif
              {{ Form::label('bill_ammount1', 'Iznos na računu 1') }}
            </div>


            <div class="iznos-racuna2">
              {{ Form::text('bill_ammount2', '', ['id' => 'bill_ammount2', 'class' => 'validate']) }}
              @if ($errors->has('bill_ammount2'))
                <span class="error-message">
                  {{ $errors->first('bill_ammount2') }}
                </span>
              @endif
              {{ Form::label('bill_ammount2', 'Iznos na računu 2') }}
                            <div type="button" class="question-mark" data-trigger="focus" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Unesite u formatu npr(5251) bez zareza, tačaka i decimala.">
                              <i class="fas fa-info-circle"></i>
                            </div>
            </div>
          </div>
          <div class="disclaimer">
            <em style="color: #FF0000;">*Minimalan iznos 5000 rsd, 1 ili zbir 2 računa preko 5.000rsd</em>
          </div>

        </div>
        <div>
          @if (session('error'))
            <span>
                  {{ session('error') }}
              </span>
          @endif
        </div>

        <div class="dates form-group shopping_select">
          {{ Form::label('shopping_date', 'Datum računa') }}

          <div class="date-selects">
            {{ Form::select('month', [
                'Januar' => 'Januar',
                'Februar' => 'Februar',
                'Mart' => 'Mart',
                'April' => 'April',
                'Maj' => 'Maj',
                'Jun' => 'Jun',
                'Jul' => 'Jul',
                'Avgust' => 'Avgust',
                'Septembar' => 'Septembar',
                'Oktobar' => 'Oktobar',
                'Novembar' => 'Novembar',
                'Decembar' => 'Decembar'
            ], 'null', ['id' => 'month','class' => 'form-control', 'placeholder' => 'Mesec']) }}
            @if ($errors->has('month'))
              <span class="error-message error-month">
                {{ $errors->first('month') }}
              </span>
            @endif
            {{ Form::select('day', [
                0 => 0,
                1 => 1,
                2 => 2,
                3 => 3,
                4 => 4,
                5 => 5,
                6 => 6,
                7 => 7,
                8 => 8,
                9 => 9,
                10 => 10,
                11 => 11,
                12 => 12,
                13 => 13,
                14 => 14,
                15 => 15,
                16 => 16,
                17 => 17,
                18 => 18,
                19 => 19,
                20 => 20,
                21 => 21,
                22 => 22,
                23 => 23,
                24 => 24,
                25 => 25,
                26 => 26,
                27 => 27,
                28 => 28,
                29 => 29,
                30 => 30,
                31 => 31
            ], 'null', ['id' => 'day','class' => 'form-control', 'placeholder' => 'Dan']) }}
            @if ($errors->has('day'))
              <span class="error-message error-day">
                {{ $errors->first('day') }}
              </span>
            @endif
            {{ Form::select('year', [
                2018 => 2018,
                2019 => 2019,
            ], 'null', ['id' => 'year','class' => 'form-control', 'placeholder' => 'Godina']) }}
            @if ($errors->has('year'))
              <span class="error-message error-year">
                {{ $errors->first('year') }}
              </span>
            @endif
          </div>
        </div>

        <div class="form-group shopping_select">
          {{ Form::label('shoping_place', 'Prodavnica') }}
          {{ Form::select('shoping_place', [
              'Idea' => 'Idea',
              'TOP SHOP' => 'TOP SHOP',
              'dm' => 'dm',
              'Pop S' => 'Pop S',
              'Four M' => 'Four M',
              'Natura Siberica' => 'Natura Siberica',
              'Cvetni Atelje' => 'Cvetni Atelje',
              'Benu' => 'Benu',
              'Zara Home' => 'Zara Home',
              'Sephora' => 'Sephora',
              "Women's Secret" => "Women's Secret",
              'Travel House' => 'Travel House',
              'Golden Point' => 'Golden Point',
              'MAC' => 'MAC',
              'Zara' => 'Zara',
              'Mango' => 'Mango',
              'Accessorize' => 'Accessorize',
              'Watch Planet' => 'Watch Planet',
              'Massimo Dutti - Muški' => 'Massimo Dutti - Muški',
              'Massimo Dutti- Ženski' => 'Massimo Dutti- Ženski',
              'Benetton' => 'Benetton',
              'XYZ' => 'XYZ',
              'Diopta' => 'Diopta',
              'Nespresso' => 'Nespresso',
              'Pandora' => 'Pandora',
              'Piquadro' => 'Piquadro',
              'Zlatara Stefanovic' => 'Zlatara Stefanovic',
              "L'occitane" => "L'occitane",
              'Zlatarska Kuca Andrejevic' => 'Zlatarska Kuca Andrejevic',
              'Diesel' => 'Diesel',
              'New Yorker' => 'New Yorker',
              'Glam Up' => 'Glam Up',
              'Feedback' => 'Feedback',
              'Samsung Mobile' => 'Samsung Mobile',
              'Parfois' => 'Parfois',
              'Coffee Room' => 'Coffee Room',
              'Oysho' => 'Oysho',
              'Cortefiel' => 'Cortefiel',
              'Jasmin' => 'Jasmin',
              'Office Shoes' => 'Office Shoes',
              'Max Mara Weekend' => 'Max Mara Weekend',
              'Moj Kiosk' => 'Moj Kiosk',
              'Zaks' => 'Zaks',
              'Swarowski' => 'Swarowski',
              'Fashion&Friends' => 'Fashion&Friends',
              'Timberland' => 'Timberland',
              'Tommy Hilfiger' => 'Tommy Hilfiger',
              'Springfield' => 'Springfield',
              'La Coste' => 'La Coste',
              "Levi's" => "Levi's",
              'Lindex' => 'Lindex',
              'Terranova' => 'Terranova',
              'Stradivarius' => 'Stradivarius',
              'Pull & Bear' => 'Pull & Bear',
              'Bershka' => 'Bershka',
              'PS fashion' => 'PS fashion',
              'Koton' => 'Koton',
              'Deichmann' => 'Deichmann',
              'C&A' => 'C&A',
              'Vip Mobile' => 'Vip Mobile',
              'Ecco' => 'Ecco',
              'Monument' => 'Monument',
              'Fly London' => 'Fly London',
              'BPM Watch' => 'BPM Watch',
              'Extreme Intimo' => 'Extreme Intimo',
              'Bomar' => 'Bomar',
              'Orsay' => 'Orsay',
              'Lisca' => 'Lisca',
              'Champion' => 'Champion',
              'Nike' => 'Nike',
              'Under Armour' => 'Under Armour',
              'Sport Vision' => 'Sport Vision',
              'Adidas' => 'Adidas',
              'Converse' => 'Converse',
              'Buzz' => 'Buzz',
              'Garinello' => 'Garinello',
              'BS' => 'BS',
              'Desigual' => 'Desigual',
              'Fratelli e Amici' => 'Fratelli e Amici',
              'Via Spiga' => 'Via Spiga',
              'Guess' => 'Guess',
              'CK Underwear' => 'CK Underwear',
              'Yves Rocher' => 'Yves Rocher',
              'Samsonite' => 'Samsonite',
              'Manual' => 'Manual',
              'Tudors' => 'Tudors',
              'Zlatarna Celje' => 'Zlatarna Celje',
              'Labrador' => 'Labrador',
              'US Polo' => 'US Polo',
              'Bata' => 'Bata',
              'Tom Tailor' => 'Tom Tailor',
              'Nursace' => 'Nursace',
              'Gagliardi' => 'Gagliardi',
              'Nine West' => 'Nine West',
              'Carpisa' => 'Carpisa',
              'Barbolini' => 'Barbolini',
              'Geox' => 'Geox',
              'N Fashion' => 'N Fashion',
              'Cineplexx' => 'Cineplexx',
              'Com Trade' => 'Com Trade',
              'Gigatron' => 'Gigatron',
              'Dexy Co Kids' => 'Dexy Co Kids',
              'Eventim' => 'Eventim',
              'LC WAIKIKI' => 'LC WAIKIKI',
              'Ciciban' => 'Ciciban',
              'OshKosh & Carters' => 'OshKosh & Carters',
              'Café Doncafe' => 'Café Doncafe',
              'Original Marines' => 'Original Marines',
              'Wizard' => 'Wizard',
              'Casina Favorit' => 'Casina Favorit',
              'In Medio' => 'In Medio',
              'Walter' => 'Walter',
              'Vapiano' => 'Vapiano',
              'Food Square' => 'Food Square',
              'Vulkan Knjizare' => 'Vulkan Knjizare',
              'Beosport' => 'Beosport',
              'Raiffeisen Banka' => 'Raiffeisen Banka',
              'Lilly' => 'Lilly',
              'Novo Lux' => 'Novo Lux',
              'Casa Bianca' => 'Casa Bianca',
              'Huawei experience store' => 'Huawei experience store',
              'Samsung Store' => 'Samsung Store',
              'iStyle' => 'iStyle',
              'Poslovnica Telekoma Srbija "Usce"' => 'Poslovnica Telekoma Srbija "Usce"',
              'Games' => 'Games',
              'HP Brand Store - Beograd' => 'HP Brand Store - Beograd',
              'Mi Store' => 'Mi Store',
              'Telenor' => 'Telenor',
              'Optiplaza' => 'Optiplaza',
              'Beba Kids' => 'Beba Kids',
              'ARS' => 'ARS',
              'Mothercare and Early Learning Center' => 'Mothercare and Early Learning Center',
              'Big Pizza' => 'Big Pizza',
              'Fish & Bar' => 'Fish & Bar',
              'Dva Stapica' => 'Dva Stapica',
              'Foody' => 'Foody',
              'KFC' => 'KFC',
              'Pasta House' => 'Pasta House',
              "McDonald's" => "McDonald's",
              'Smart Arena' => 'Smart Arena',
              'Home Plus' => 'Home Plus',
              'Win Win' => 'Win Win',
              'Epilion' => 'Epilion',
              'Sportska Akademija Kocovic' => 'Sportska Akademija Kocovic',
              'Foto Toto' => 'Foto Toto',
              'Proteini Si' => 'Proteini Si',
              'Cold Pressok' => 'Cold Pressok',
              'Hemel' => 'Hemel',
              'Medisana' => 'Medisana',
              'Sweet Room' => 'Sweet Room',
              'E-Feel' => 'E-Feel',
              'SBB' => 'SBB',
              'Mani Cool' => 'Mani Cool',
              'In Mobile' => 'In Mobile',
              'Small Tree' => 'Small Tree',
              'Adore' => 'Adore',
              'Galaxy Code' => 'Galaxy Code',
              'Vip Poklon' => 'Vip Poklon',
              'Flormar' => 'Flormar',
              'Flex' => 'Flex',
              'Serbia Art' => 'Serbia Art',
              'Mascom Record Store' => 'Mascom Record Store',
              'Phillip Morris' => 'Phillip Morris',
              'Wish' => 'Wish',
              'Oxette' => 'Oxette',
              'Mobile Outfitters' => 'Mobile Outfitters',
              "C'est Moi" => "C'est Moi",
              'Studio Maruška' => 'Studio Maruška',
              'Studio 2' => 'Studio 2',
              'Autoškola' => 'Autoškola',
              'Umbrella' => 'Umbrella',
              'Aqua Mineral' => 'Aqua Mineral',
              'NYX' => 'NYX',
              'Crocs' => 'Crocs',
              '3G' => '3G',
              'Optiplaza' => 'Optiplaza',
              'Bomar' => 'Bomar',
              'Shop&Service' => 'Shop&Service',
              'Candy universe' => 'Candy universe',
              'Elixir Bar' => 'Elixir Bar',
              'Cyber Shark' => 'Cyber Shark',
              'Pharmasept' => 'Pharmasept',
              'Spring Air' => 'Spring Air',
              'I Clay' => 'I Clay',
              'Bubble Tea' => 'Bubble Tea',
              'Yo Mama' => 'Yo Mama',
              'Moritz Eis' => 'Moritz Eis',
              'VR & FUN' => 'VR & FUN',
              'Games4you' => 'Games4you',
              'Urban Food' => 'Urban Food',
              'Expedition' => 'Expedition',
              'DremCo' => 'DremCo',
              'Glo' => 'Glo',
              'Sas monitoring' => 'Sas monitoring'
          ], 'null', ['id' => 'shoping_place','class' => 'form-control', 'placeholder' => 'Izaberite prodavnicu']) }}
          @if ($errors->has('shoping_place'))
            <span class="error-message">
              {{ $errors->first('shoping_place') }}
            </span>
          @endif

        </div>

        <div class="dates form-group shopping_select2">
          {{ Form::label('shopping_date', 'Datum drugog računa') }}

          <div class="date-selects">
            {{ Form::select('month_two', [
                'Januar' => 'Januar',
                'Februar' => 'Februar',
                'Mart' => 'Mart',
                'April' => 'April',
                'Maj' => 'Maj',
                'Jun' => 'Jun',
                'Jul' => 'Jul',
                'Avgust' => 'Avgust',
                'Septembar' => 'Septembar',
                'Oktobar' => 'Oktobar',
                'Novembar' => 'Novembar',
                'Decembar' => 'Decembar'
            ], 'null', ['id' => 'month_two','class' => 'form-control', 'placeholder' => 'Mesec']) }}

            {{ Form::select('day_two', [
                0 => 0,
                1 => 1,
                2 => 2,
                3 => 3,
                4 => 4,
                5 => 5,
                6 => 6,
                7 => 7,
                8 => 8,
                9 => 9,
                10 => 10,
                11 => 11,
                12 => 12,
                13 => 13,
                14 => 14,
                15 => 15,
                16 => 16,
                17 => 17,
                18 => 18,
                19 => 19,
                20 => 20,
                21 => 21,
                22 => 22,
                23 => 23,
                24 => 24,
                25 => 25,
                26 => 26,
                27 => 27,
                28 => 28,
                29 => 29,
                30 => 30,
                31 => 31
            ], 'null', ['id' => 'day_two','class' => 'form-control', 'placeholder' => 'Dan']) }}

            {{ Form::select('year_two', [
                2018 => 2018,
                2019 => 2019,
            ], 'null', ['id' => 'year_two','class' => 'form-control', 'placeholder' => 'Godina']) }}

          </div>
        </div>

        <div class="form-group shopping_select shopping_select2">
          {{ Form::label('shoping_place2', 'Prodavnica 2') }}
          {{ Form::select('shoping_place2', [
              'Idea' => 'Idea',
              'TOP SHOP' => 'TOP SHOP',
              'dm' => 'dm',
              'Pop S' => 'Pop S',
              'Four M' => 'Four M',
              'Natura Siberica' => 'Natura Siberica',
              'Cvetni Atelje' => 'Cvetni Atelje',
              'Benu' => 'Benu',
              'Zara Home' => 'Zara Home',
              'Sephora' => 'Sephora',
              "Women's Secret" => "Women's Secret",
              'Travel House' => 'Travel House',
              'Golden Point' => 'Golden Point',
              'MAC' => 'MAC',
              'Zara' => 'Zara',
              'Mango' => 'Mango',
              'Accessorize' => 'Accessorize',
              'Watch Planet' => 'Watch Planet',
              'Massimo Dutti - Muški' => 'Massimo Dutti - Muški',
              'Massimo Dutti- Ženski' => 'Massimo Dutti- Ženski',
              'Benetton' => 'Benetton',
              'XYZ' => 'XYZ',
              'Diopta' => 'Diopta',
              'Nespresso' => 'Nespresso',
              'Pandora' => 'Pandora',
              'Piquadro' => 'Piquadro',
              'Zlatara Stefanovic' => 'Zlatara Stefanovic',
              "L'occitane" => "L'occitane",
              'Zlatarska Kuca Andrejevic' => 'Zlatarska Kuca Andrejevic',
              'Diesel' => 'Diesel',
              'New Yorker' => 'New Yorker',
              'Glam Up' => 'Glam Up',
              'Feedback' => 'Feedback',
              'Samsung Mobile' => 'Samsung Mobile',
              'Parfois' => 'Parfois',
              'Coffee Room' => 'Coffee Room',
              'Oysho' => 'Oysho',
              'Cortefiel' => 'Cortefiel',
              'Jasmin' => 'Jasmin',
              'Office Shoes' => 'Office Shoes',
              'Max Mara Weekend' => 'Max Mara Weekend',
              'Moj Kiosk' => 'Moj Kiosk',
              'Zaks' => 'Zaks',
              'Swarowski' => 'Swarowski',
              'Fashion&Friends' => 'Fashion&Friends',
              'Timberland' => 'Timberland',
              'Tommy Hilfiger' => 'Tommy Hilfiger',
              'Springfield' => 'Springfield',
              'La Coste' => 'La Coste',
              "Levi's" => "Levi's",
              'Lindex' => 'Lindex',
              'Terranova' => 'Terranova',
              'Stradivarius' => 'Stradivarius',
              'Pull & Bear' => 'Pull & Bear',
              'Bershka' => 'Bershka',
              'PS fashion' => 'PS fashion',
              'Koton' => 'Koton',
              'Deichmann' => 'Deichmann',
              'C&A' => 'C&A',
              'Vip Mobile' => 'Vip Mobile',
              'Ecco' => 'Ecco',
              'Monument' => 'Monument',
              'Fly London' => 'Fly London',
              'BPM Watch' => 'BPM Watch',
              'Extreme Intimo' => 'Extreme Intimo',
              'Bomar' => 'Bomar',
              'Orsay' => 'Orsay',
              'Lisca' => 'Lisca',
              'Champion' => 'Champion',
              'Nike' => 'Nike',
              'Under Armour' => 'Under Armour',
              'Sport Vision' => 'Sport Vision',
              'Adidas' => 'Adidas',
              'Converse' => 'Converse',
              'Buzz' => 'Buzz',
              'Garinello' => 'Garinello',
              'BS' => 'BS',
              'Desigual' => 'Desigual',
              'Fratelli e Amici' => 'Fratelli e Amici',
              'Via Spiga' => 'Via Spiga',
              'Guess' => 'Guess',
              'CK Underwear' => 'CK Underwear',
              'Yves Rocher' => 'Yves Rocher',
              'Samsonite' => 'Samsonite',
              'Manual' => 'Manual',
              'Tudors' => 'Tudors',
              'Zlatarna Celje' => 'Zlatarna Celje',
              'Labrador' => 'Labrador',
              'US Polo' => 'US Polo',
              'Bata' => 'Bata',
              'Tom Tailor' => 'Tom Tailor',
              'Nursace' => 'Nursace',
              'Gagliardi' => 'Gagliardi',
              'Nine West' => 'Nine West',
              'Carpisa' => 'Carpisa',
              'Barbolini' => 'Barbolini',
              'Geox' => 'Geox',
              'N Fashion' => 'N Fashion',
              'Cineplexx' => 'Cineplexx',
              'Com Trade' => 'Com Trade',
              'Gigatron' => 'Gigatron',
              'Dexy Co Kids' => 'Dexy Co Kids',
              'Eventim' => 'Eventim',
              'LC WAIKIKI' => 'LC WAIKIKI',
              'Ciciban' => 'Ciciban',
              'OshKosh & Carters' => 'OshKosh & Carters',
              'Café Doncafe' => 'Café Doncafe',
              'Original Marines' => 'Original Marines',
              'Wizard' => 'Wizard',
              'Casina Favorit' => 'Casina Favorit',
              'In Medio' => 'In Medio',
              'Walter' => 'Walter',
              'Vapiano' => 'Vapiano',
              'Food Square' => 'Food Square',
              'Vulkan Knjizare' => 'Vulkan Knjizare',
              'Beosport' => 'Beosport',
              'Raiffeisen Banka' => 'Raiffeisen Banka',
              'Lilly' => 'Lilly',
              'Novo Lux' => 'Novo Lux',
              'Casa Bianca' => 'Casa Bianca',
              'Huawei experience store' => 'Huawei experience store',
              'Samsung Store' => 'Samsung Store',
              'iStyle' => 'iStyle',
              'Poslovnica Telekoma Srbija "Usce"' => 'Poslovnica Telekoma Srbija "Usce"',
              'Games' => 'Games',
              'HP Brand Store - Beograd' => 'HP Brand Store - Beograd',
              'Mi Store' => 'Mi Store',
              'Telenor' => 'Telenor',
              'Optiplaza' => 'Optiplaza',
              'Beba Kids' => 'Beba Kids',
              'ARS' => 'ARS',
              'Mothercare and Early Learning Center' => 'Mothercare and Early Learning Center',
              'Big Pizza' => 'Big Pizza',
              'Fish & Bar' => 'Fish & Bar',
              'Dva Stapica' => 'Dva Stapica',
              'Foody' => 'Foody',
              'KFC' => 'KFC',
              'Pasta House' => 'Pasta House',
              "McDonald's" => "McDonald's",
              'Smart Arena' => 'Smart Arena',
              'Home Plus' => 'Home Plus',
              'Win Win' => 'Win Win',
              'Epilion' => 'Epilion',
              'Sportska Akademija Kocovic' => 'Sportska Akademija Kocovic',
              'Foto Toto' => 'Foto Toto',
              'Proteini Si' => 'Proteini Si',
              'Cold Pressok' => 'Cold Pressok',
              'Hemel' => 'Hemel',
              'Medisana' => 'Medisana',
              'Sweet Room' => 'Sweet Room',
              'E-Feel' => 'E-Feel',
              'SBB' => 'SBB',
              'Mani Cool' => 'Mani Cool',
              'In Mobile' => 'In Mobile',
              'Small Tree' => 'Small Tree',
              'Adore' => 'Adore',
              'Galaxy Code' => 'Galaxy Code',
              'Vip Poklon' => 'Vip Poklon',
              'Flormar' => 'Flormar',
              'Flex' => 'Flex',
              'Serbia Art' => 'Serbia Art',
              'Mascom Record Store' => 'Mascom Record Store',
              'Phillip Morris' => 'Phillip Morris',
              'Wish' => 'Wish',
              'Oxette' => 'Oxette',
              'Mobile Outfitters' => 'Mobile Outfitters',
              "C'est Moi" => "C'est Moi",
              'Studio Maruška' => 'Studio Maruška',
              'Studio 2' => 'Studio 2',
              'Autoškola' => 'Autoškola',
              'Umbrella' => 'Umbrella',
              'Aqua Mineral' => 'Aqua Mineral',
              'NYX' => 'NYX',
              'Crocs' => 'Crocs',
              '3G' => '3G',
              'Optiplaza' => 'Optiplaza',
              'Bomar' => 'Bomar',
              'Shop&Service' => 'Shop&Service',
              'Candy universe' => 'Candy universe',
              'Elixir Bar' => 'Elixir Bar',
              'Cyber Shark' => 'Cyber Shark',
              'Pharmasept' => 'Pharmasept',
              'Spring Air' => 'Spring Air',
              'I Clay' => 'I Clay',
              'Bubble Tea' => 'Bubble Tea',
              'Yo Mama' => 'Yo Mama',
              'Moritz Eis' => 'Moritz Eis',
              'VR & FUN' => 'VR & FUN',
              'Games4you' => 'Games4you',
              'Urban Food' => 'Urban Food',
              'Expedition' => 'Expedition',
              'DremCo' => 'DremCo',
              'Glo' => 'Glo',
              'Sas monitoring' => 'Sas monitoring'
          ], 'null', ['id' => 'shoping_place2','class' => 'form-control', 'placeholder' => 'Izaberite prodavnicu']) }}
          @if ($errors->has('shoping_place2'))
            <span class="error-message">
              {{ $errors->first('shoping_place2') }}
            </span>
          @endif

        </div>

        <div class="input-field posalji">
          {{ Form::submit('Pošalji', ['class' => 'submit_btn']) }}
        </div>
        {!! Form::close() !!}
        <div class="input-field posalji posalji2">
          <button class="submit_btn submit_btn2">Pošalji</button>
        </div>
      </div>
    </div>
  </div>

  <div class="register-bottom-logo">
    <div class="disclaimer">
      <em style="color: #FF0000;">*Svi registrovani korisnici će dobiti email obaveštenja sa jedinstvenim registracionim ID brojem.</em>
    </div>
    <img src="{{asset('/img/footer_logo.png')}}" alt="">
  </div>

@section('javascript')
  <script>
    $(function() {
      $("[data-toggle=\"popover\"]").popover();
    });
    $("#broj_racuna[data-toggle=\"popover\"], #broj_racuna2[data-toggle=\"popover\"]").popover({
      html: true,
      content: "<img src='./img/racun.jpg' class='img-fluid'>"
    });

    // Set initial value of phone input
    document.querySelector("#phone").addEventListener("focus", function() {
      this.value = "+381";
    });

    var shoppingSelect2 = document.querySelectorAll('.shopping_select2');


    // Show bill 2 if value of bill 1 is equal or lower then 5000
    document.querySelector('#bill_ammount1').addEventListener('input', function() {
      if (this.value < 5000 && this.value.length > 1) {
        // document.querySelector('#bill_number2').style.display = 'block';
        document.querySelector('.racun2').style.display = 'block';
        // document.querySelector('#bill_ammount2').style.display = 'block';
        shoppingSelect2.forEach(function(item) {
          item.style.display = 'block'
        });
        // document.querySelectorAll('.shopping_select2').style.display = 'block';
      } else {
        // document.querySelector('#bill_number2').style.display = 'none';
        document.querySelector('.racun2').style.display = 'none';
        // document.querySelector('#bill_ammount2').style.display = 'none';
        shoppingSelect2.forEach(function(item) {
          item.style.display = 'none'
        });
      }
    });

    // Instantiate variables
    var billNumberTwo = document.getElementById("bill_number2"),
      billAmountTwo = document.getElementById("bill_ammount2");

    document.querySelector(".submit_btn2").addEventListener("click", function() {
      document.querySelector(".submit_btn").click();
    });

    document.querySelectorAll(".register_form input").forEach(function(item) {
      item.addEventListener("focus", function() {
        document.querySelectorAll(".error-message").forEach(function(msg) {
          msg.style.display = "none";
        });
      });
    });

    document.querySelectorAll(".register_form select").forEach(function(item) {
      item.addEventListener("focus", function() {
        document.querySelectorAll(".error-message").forEach(function(msg) {
          msg.style.display = "none";
        });
      });
    });

  </script>
@endsection
@endsection
