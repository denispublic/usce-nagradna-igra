@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="winners-container">
      <div class="winners-background-left">
        <img src="{{asset('/img/winners-mob-cover.png')}}" class="winners-mob-cover" alt="winners-mob">
      </div>
      <div class="winners-list-container">
        <div class="search_container">
          {{--        <h1 class="text-white winners-list-heading text-uppercase">Lista dobitnika</h1>--}}
        </div>
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link bg_green switch active" id="pills-daily-tab" data-toggle="pill" href="#pills-daily"
               role="tab" aria-controls="pills-daily" aria-selected="true" data-input-id="js-winners">INSTANT NAGRADE</a>
          </li>
          <li class="nav-item">
            <a class="nav-link bg_green switch" id="pills-tendaily-tab" data-toggle="pill" href="#pills-tendaily" role="tab"
               aria-controls="pills-tendaily" aria-selected="false" data-input-id="js-winners2">VAUČER OD 111.000 RSD</a>
          </li>
          <li class="nav-item">
            <a class="nav-link bg_orange switch" id="pills-main-tab" data-toggle="pill" href="#pills-main" role="tab"
               aria-controls="pills-main" aria-selected="false"  data-input-id="js-winners3">GLAVNA NAGRADA</a>
          </li>
          <li class="nav-item">
            <a class="nav-link bg_orange switch" id="pills-putovanja-tab" data-toggle="pill" href="#pills-putovanja" role="tab"
               aria-controls="pills-putovanja" aria-selected="false" data-input-id="js-winners4">PUTOVANJA</a>
          </li>
        </ul>

        <div class="tab-content" id="pills-tabContent">
          <div class="tab-pane fade show active daily-winner" id="pills-daily" role="tabpanel"
               aria-labelledby="pills-daily-tab" data-input-id="js-winners">
            <table class="table" cellspacing="0" width="100%">
              <thead>
              <tr>
                <th class="th-sm">Ime dobitnika</th>
                <th class="th-sm">Nagrada</th>
                <th class="th-sm">ID Broj</th>
              </tr>
              </thead>
              <tbody>
              @foreach($dailyWinners as $dailyWinner)
                <tr>
                  <td>{{ $dailyWinner->user->firstname }} {{ $dailyWinner->user->lastname }}</td>
                  <td>{{ number_format($dailyWinner->ammount, '2', ',', '.') }} RSD</td>
                  <td>{{ $dailyWinner->user->generated_id }}</td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
          <div class="tab-pane fade" id="pills-tendaily" role="tabpanel"
               aria-labelledby="pills-tendaily-tab" data-input-id="js-winners2">
            <table class="table" cellspacing="0" width="100%">
              <thead>
              <tr>
                <th class="th-sm">Ime dobitnika</th>
                <th class="th-sm">Nagrada</th>
                <th class="th-sm">ID Broj</th>
              </tr>
              </thead>
              <tbody>
              @foreach($tenDayWinners as $tenDayWinner)
                <tr>
                  <td>{{ $tenDayWinner->user->firstname }} {{ $tenDayWinner->user->lastname }}</td>
                  <td>{{ number_format($tenDayWinner->ammount, '2', ',', '.') }} RSD</td>
                  <td>{{ $tenDayWinner->user->generated_id }}</td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
          <div class="tab-pane fade main-winner js-winners3" id="pills-main" role="tabpanel" aria-labelledby="pills-main-tab" data-input-id="js-winners3">
            <table class="table" cellspacing="0" width="100%">
              <thead>
              <tr>
                <th class="th-sm">Ime dobitnika</th>
                <th class="th-sm">Nagrada</th>
                <th class="th-sm">ID Broj</th>
              </tr>
              </thead>
              <tbody>
              @foreach($mainWinner as $winner)
                <tr>
                  <td>{{ $winner->user->firstname }} {{ $winner->user->lastname }}</td>
                  <td>Putovanje u Los Anđeles</td>
                  <td>{{ $winner->user->generated_id }}</td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
          <div class="tab-pane fade london-winner" id="pills-putovanja" role="tabpanel" aria-labelledby="pills-putovanja-tab" data-input-id="js-winners4">
            <table class="table" cellspacing="0" width="100%">
              <thead>
              <tr>
                <th class="th-sm">Ime dobitnika</th>
                <th class="th-sm">Nagrada</th>
                <th class="th-sm">ID Broj</th>
              </tr>
              </thead>
              <tbody>
              @foreach($londonWinner as $winner)
                <tr>
                  <td>{{ $winner->user->firstname }} {{ $winner->user->lastname }}</td>
                  <td>Putovanje u London</td>
                  <td>{{ $winner->user->generated_id }}</td>
                </tr>
              @endforeach
              @foreach($madridWinner as $winner)
                <tr>
                  <td>{{ $winner->user->firstname }} {{ $winner->user->lastname }}</td>
                  <td>Putovanje u Madrid</td>
                  <td>{{ $winner->user->generated_id }}</td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="winners-background-right"></div>
    </div>
  </div>
  <div class="register-bottom-logo">
    <img src="{{asset('/img/footer_logo.png')}}" alt="">
  </div>

@section('javascript')
  <script>
    var $ = jQuery.noConflict();
    $(document).ready(function() {
      $(".table").DataTable({
        "pagingType": "simple_numbers",
        "lengthChange": false,
        "info": false,
        "language": { search: "" },
        "bInfo": false
      });

      $(".dataTables_length").addClass("bs-select");

      var template = $("input.form-control.form-control-sm");
      template.each(function() {
        var id = $(this).closest(".tab-pane").attr("data-input-id");
        $(this).attr("data-input-id", id);
      });

      $(".search_container").append(template);

      $(".switch").click(function() {
        var id = $(this).attr("data-input-id");
        $("input[type=search]").hide();
        $("input[data-input-id='" + id + "']").show();
      });

      $("input[type=search]").hide();
      $("input[data-input-id='js-winners']").show();

      $(".form-control.form-control-sm").attr("placeholder", "pronađite svoj ID broj format: 016xxxx");
    });
  </script>
@endsection
@endsection
