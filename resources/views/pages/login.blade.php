@extends('layouts.app')

@section('content')
    <div class="login_title">
        <span class="text-white text-uppercase bg_green">Ulogujte se</span>
    </div>

    {!! Form::open(['action' => 'LoginController@login', 'method' => 'POST', 'class' => 'login_form']) !!}
        {{ Form::text('username', '', ['id' => 'username', 'placeholder' => 'Korisničko ime']) }}
        @if ($errors->has('username'))
            <span>
                {{ $errors->first('username') }}
            </span>
        @endif
        {{ Form::password('password', ['id' => 'password', 'placeholder' => 'Lozinka', 'type' => 'password']) }}
        @if ($errors->has('password'))
            <span>
                {{ $errors->first('password') }}
            </span>
        @endif
        @if (session('error'))
            <span>
                {{ session('error') }}
            </span>
        @endif
        {{ Form::submit('Prijava', ['class' => 'submit_btn mx-0 my-3']) }}
    {!! Form::close() !!}
@endsection
