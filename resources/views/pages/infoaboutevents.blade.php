@extends('layouts.app')

@section('content')

<div class="container">
  <div class="afterhours_headline">
        <h1 class="text-white home-page text-uppercase">Velika Ušće nagradna igra</h1>
      </div>
  <div class="row">
    <div class="container">
      <p class="text-white text-center rules_head__content">UKOLIKO STE ZAINTERESOVANI DA BUDETE UČESNIK NAGRADNE IGRE „Ušće rođendanski match“, MOLIMO VAS DA PROČITATE SLEDEĆE INFORMACIJE I DATE SVOJ PRISTANAK</p>
      <p class="text-white">U nameri da zaštitimo Vaše pravo na privatnost koje je zagarantovano Zakonom o zaštiti podataka o ličnosti želimo da Vas obavestimo o sledećem:</p>
      <span class="text-uppercase rules_head__title">Vaše lične podatke obrađuje i stara se o njihovoj bezbednosti:</span>
      <h2 class="text-white text-uppercase rules_title">Ušće Shopping Center d.o.o Beograd</h2>
      <p class="text-white">Adresa: Bulevar Mihaila Pupina 6, 11070 Beograd, Srbija</p>
      <p class="text-white">e-mail:<a href="mailto:info@immooutlet.com"> info@immooutlet.com</a></p>
      <p class="text-white">Matični broj: 20567716</p>
      <div class="rules_list__container">
        <p class="text-white">Svrha prikupljanja Vaših ličnih podataka je učešće u nagradnoj igri „Ušće rođendanski match“, koja počinje 16. marta 2020. godine i traje do 12. aprila 2020. godine. Vaše podatke ćemo koristiti samo u cilju ispunjenja obaveza koje proističu iz javno obećanih nagrada iz zakona koji reguliše obligacione (ugovorne) odnose i organizovanja nagradnih igara iz Zakona o igrama na sreću (evidentiranje učesnika u nagradnoj igri, u cilju: sprovođenja nagradne igre (odabir dobitnika i dodele nagrada istim), sprečavanja zloupotreba i kršenja Pravila nagradne igre, obezbeđenja dokaza o predatim nagradama i poštovanja propisa Republike Srbije), a na osnovu Vašeg pristanka na obradu podataka, regulisanog Zakonom o zaštiti podataka o ličnosti i čuvati do ispunjenja svrhe, odnosno do završetka nagradne igre, s tim da ćemo podatke o dobitnicima čuvati u roku koje je određen pozitivnim propisima Republike Srbije. Vaši lični podaci će biti otkriveni isključivo privrednom društvu Confluence Property Management d.o.o. Beograd, kao našem partneru koji organizuje i sprovodi ovu nagradnu igru i nadležnim državnim organima.</p>
        <p class="text-white">U cilju zaštite Vašeg prava na privatnost obaveštavamo Vas da imate:</p>
        <ol class="rules_list first-list">
          <li class="text-white">pravo na pristup Vašim podacima; </li>
          <li class="text-white">pravo da od nas tražite ispravku Vaših podataka;</li>
          <li class="text-white">pravo da od nas tražite da izbrišemo Vaše podatke;</li>
          <li class="text-white">pravo da opozovete pristanak na obradu Vaših podataka i to na osnovu zahteva podnetog na sledeću e-mail adresu: <a href="mailto:info@usceshoppingcenter.com">info@usceshoppingcenter.com</a>; s tim da će Vam u tom slučaju biti onemogućeno dalje učestvovanje u nagradnoj igri zbog nemogućnosti ispunjavanja osnovnih uslova i svrhe nagradne igre, a da Ušće Shopping Center d.o.o Beograd ima pravo na naknadu opravdanih troškova i štete pretrpljene zbog takvog povlačenja;</li>
          <li class="text-white">pravo da ograničite obradu Vaših podataka;</li>
          <li class="text-white">pravo da uložite prigovor ukoliko smatrate da je Vaše pravo na privatnost povređeno i pravo da podnesete pritužbu Povereniku za informacije od javnog značaja i zaštitu podataka o ličnosti. </li>
        </ol>
        <p>Bez uticaja na pravo učestvovanja u nagradnoj igri, saglasan/-a sam da se navedeni podaci, i to: ime i prezime, broj telefona, email adresa i nadimak (i JMBG, broj lične karte i adresa u slučaju dobitnika) mogu koristiti kako bih primao/la obaveštenja o događajima koji se organizuju u okviru Ušće Shopping Center, i to događaja koji je odnose na: proslave rođendana, dane kupovine i specijalne pogodnosti za kupovinu, organizovanje nagradnih igara, letnje projekte, edukativne događaje (predavanja i govori), projekte koji se odnose na povratak u školu, projekte koji se odnose na proslavu Nove godine i Božića (Čarobni grad), Uskrs, 8. mart i Dan zaljubljenih.</p>
        <p>U slučaju da ste dali pristanak da Vas obaveštavamo o navedenim događajima, bez obzira na konkretan naziv navedenog događaja, Vaši lični podaci će biti čuvani 5 (pet) godina od dana prikupljanja.</p>
        <p>Sva gore navedena prava (uključujući, ali ne ograničavajući se na pravo na opoziv) možete iskoristiti i na obradu podataka za svrhu za koju ste dali pristanak u tabeli.</p>
        <p>Podaci koje nam dostavite biće čuvani u Republici Srbiji, uz primenu odgovarajućih tehničkih i organizacionih mera koje osiguravaju njihovu bezbednost.</p>
        <p>Klikom na odgovarajuća polja potvrđujem da sam pročitao/la i u potpunosti razumeo/la sve napred navedeno, što se ima smatrati obaveštenjem iz člana 23. Zakona o zaštiti podataka o ličnosti i da sam u potpunosti saglasan/-a sa celokupnom sadržinom.</p>

        {!! Form::open(['action' => 'HomepageController@terms', 'method' => 'POST', 'class' => 'homepage_register__form container mt-5']) !!}
          <div class="form-group">
            {{ Form::submit('Prijavi se', ['class' => 'submit_btn']) }}
          </div>
          <div class="form-group accept_terms">
          {{ Form::label('terms-and-conditions', 'Prihvatam pravila nagradne igre') }}
          {{ Form::checkbox('terms-and-conditions', true, false, ['id' => 'terms-and-conditions']) }}
          </div>
          <div class="form-group accept_info_game">
          {{ Form::checkbox('newsletter', true, false, ['id' => 'newsletter']) }}
          <label for="newsletter">Prihvatam da primam obaveštenja o događajima koji se organizuju u okviru Ušće Shopping Center-a</label>
          </div>
        {!! Form::close() !!}

        <div id="modal-terms" class="modal-terms d-none">
          <p>Morate prihvatiti pravila igre!</p>

          <a class="modal-close">
            <div class="icon-bar-1"></div>
            <div class="icon-bar-2"></div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
  <script>
    document.querySelector('.homepage_register__form .submit_btn').addEventListener('click', function(e) {
      if (document.querySelector('.accept_terms input').checked == false) {
        e.preventDefault();
        document.querySelector('.modal-terms').classList.remove('d-none');
      }
    });
    document.querySelector('.modal-terms .modal-close').addEventListener('click', function() {
      document.querySelector('.modal-terms').classList.add('d-none');
    });

  </script>

@endsection
