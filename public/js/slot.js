(function() {
  function SlotMachine(initObj, container) {
      this.initObj = initObj;
      this.container = container;
      this.init();
  }

  SlotMachine.prototype.init = function() {
      this.lines = [];

      for(var i=0; i< this.initObj.number_of_slots; i++) {
          var line = new Slot(this.initObj, this.container);
          this.lines.push(line);
      }
  };

  SlotMachine.prototype.spin = function(val, callback) {
      var arr = [];
      if(val === "3000") {
        $('.modal#modal1 .modal-content').css("background-color", "#a6ce39");
        for(var i=0; i< this.initObj.number_of_slots; i++) {
          arr[i] = 0;
        }
      } else if(val === "6000") {
        $('.modal#modal1 .modal-content').css("background-color", "#f2a11e");
        // $('.modal#modal1 .modal-content .modal_subtitle').css("color", "#f2a11e");
        // $('.modal#modal1 .modal-content .modal_subtitle').css("background-color", "#fff");
        for(var i=0; i< this.initObj.number_of_slots; i++) {
          arr[i] = 1;
        }
      } else if(val === "100000") {
        $('.modal#modal1 .modal-content').css("background-color", "#0090d7");
        for(var i=0; i< this.initObj.number_of_slots; i++) {
          arr[i] = 2;
        }
      } else {
        var same = true;

        while(same) {
          for(var i=0; i< this.initObj.number_of_slots; i++) {
            arr[i] = Math.floor(this.initObj.number_of_slots * Math.random());
          }

          // console.log(arr);

          same = true;
          var first = arr[0];
          for(i=1; i<this.initObj.number_of_slots; i++) {
            if(arr[i] < first) same = false;
          }
        }
        //todo ovde proveriti da li se slucajno poklopilo sa dobitnom kombinacijom
      }

      // console.log(arr);
      // console.log("PRE SPINA");
      // Funkcija bez koje spin nece da radi
      for(i=0; i<this.lines.length; i++) {
        this.lines[i].spin(arr[i], 5);
      }
      // console.log("POSLE SPINA");
      // console.log("pre timeouta");
      setTimeout(function() { callback(val); }, 7500);

  }

  window.SlotMachine = SlotMachine;

})();
