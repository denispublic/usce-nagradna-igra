(function() {
  function Slot(initObj, container) {
      this.initObj = initObj;
      this.container = container;

      this.init();
  }

  var lapTopScreenOnSlot = window.matchMedia('(min-width:991px) and (max-width: 1200px)');
  var phoneScreenOnSlot = window.matchMedia('(max-width: 600px)');

  Slot.prototype.init = function() {
      this.line_container = $('<div class="slot-line-container"><div class="slot-line-cover-top"></div><div class="slot-line-cover-bottom"></div></div>');
      this.container.append(this.line_container);
      this.root = $('<div class="slot-line"></div>');
      this.line_container.append(this.root);
      this.current_value = 1;

      this.one_image_height = 2 * this.initObj.padding + this.initObj.image_width;

      this.images = [];
      for(var i=0; i<this.initObj.images.length * 10; i++) {
          var image = $('<img src="' + this.initObj.images[i % this.initObj.images.length] + '" class="slot-image">');
          this.images.push(image);
          this.root.append(image);
          if(i<10 * this.initObj.images.length-1) {
              var pos = i*this.one_image_height + this.initObj.padding;
              image.css("top", pos + 80 +"px");
          }
      }


      if(lapTopScreenOnSlot.matches) {
        this.root.css("top", -(this.one_image_height) - 20 + "px");
      } else if (phoneScreenOnSlot.matches) {
        this.root.css("top", -(this.one_image_height) - 30 + "px");
      } else {
        this.root.css("top", -(this.one_image_height) + "px");
      }

  };

  Slot.prototype.spin = function(val, rounds) {
    this.next_val = val;
    var new_pos = -(rounds * this.initObj.images.length + (val - this.current_value + 1)) * this.one_image_height;
    var min = 4;
    var max = 8;
    if(lapTopScreenOnSlot.matches) {
      TweenMax.to(this.root, Math.floor(Math.random() * (+max - +min) + +min), {top: new_pos - 15});
    } else if(phoneScreenOnSlot.matches) {
      TweenMax.to(this.root, Math.floor(Math.random() * (+max - +min) + +min), {top: new_pos  - 30});
    } else {
      TweenMax.to(this.root, Math.floor(Math.random() * (+max - +min) + +min), {top: new_pos});
    }
  };

  Slot.prototype.animationComplete = function() {
    this.current_value = this.next_val;
  };

  window.Slot = Slot;
})();
