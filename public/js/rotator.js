(function() {

  function SlotMachine(initObj, container) {
      this.initObj = initObj;
      this.container = container;

      this.init();
  }

  SlotMachine.prototype.init = function() {
      this.lines = [];

      for(var i=0; i< this.initObj.number_of_slots; i++) {
          var line = new Slot(this.initObj, this.container);
          this.lines.push(line);
      }
  };

  SlotMachine.prototype.spin = function(val, callback) {

      for(i=0; i<this.lines.length; i++) {
        this.lines[i].spin(val[i], 5);
      }
    setTimeout(function() { callback(val); }, 8500);
  }

  window.SlotMachine = SlotMachine;
})();
