(function() {
  function SlotMachine(initObj, container) {
      this.initObj = initObj;
      this.container = container;

      this.init();
  }

  SlotMachine.prototype.init = function() {
      this.lines = [];

      for(var i=0; i< this.initObj.number_of_slots; i++) {
          var line = new Slot(this.initObj, this.container);
          this.lines.push(line);
      }
  };

  SlotMachine.prototype.spin = function(val) {
      // var arr = [];
      // if(val === "ammount3000") {
      //   for(var i=0; i< this.initObj.number_of_slots; i++) {
      //     arr[i] = 0;
      //   }
      // } else if(val === "ammount6000") {
      //   for(var i=0; i< this.initObj.number_of_slots; i++) {
      //     arr[i] = 1;
      //   }
      // } if(val === "ammount12000") {
      //   for(var i=0; i< this.initObj.number_of_slots; i++) {
      //     arr[i] = 2;
      //   }
      // } else {
      //   for(var i=0; i< this.initObj.number_of_slots; i++) {
      //     arr[i] = Math.floor(6 * Math.random());
      //   }

      //   //todo ovde proveriti da li se slucajno poklopilo sa dobitnom kombinacijom
      // }

      for(i=0; i<this.lines.length; i++) {
        this.lines[i].spin(val[i], 5);
      }
  }

  window.SlotMachine = SlotMachine;
})();
