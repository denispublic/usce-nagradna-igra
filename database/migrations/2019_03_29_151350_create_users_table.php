<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('generated_id')->unique();
            $table->string('email');
            $table->string('phone_number');
            $table->string('bill_number1');
            $table->string('bill_number2')->nullable();
            $table->string('bill_ammount1');
            $table->string('bill_ammount2')->nullable();
            $table->string('shoping_place');
            $table->string('shoping_place2')->nullable();
            $table->boolean('winner');
            $table->datetime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
