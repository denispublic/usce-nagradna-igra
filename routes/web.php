<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

// Login
Route::get('/login', 'LoginController@getView');
Route::post('/login', 'LoginController@login');

// Dashboard
Route::get('/dashboard', 'DashboardController@getView');
Route::post('/dashboard', 'LogoutController@logout');

// Homepage
//Route::get('/', 'HomepageController@getView');
//Route::post('/', 'HomepageController@terms');

//Homepage - server
 Route::get('/', 'HomepageController@getView')->name('homepage');
 Route::post('https://dev1.m1.rs/usce-jesen/public/', 'HomepageController@terms');


// Register
Route::get('/prijava', 'RegisterController@getView');
Route::post('/prijava', 'RegisterController@register');



// 10 Day Winner
//Route::get('/izvlacenje-desetodnevnog-dobitnika', 'TenDailyWinnersController@tenDayWinners');
//Route::get('/tendaywinnerjson', 'TenDailyWinnerJsonController@getWinner');

// ** IZVLACENJE NAGRADA **

// DNEVNE NAGRADE
Route::get('/igra', 'LuckyJackController@getView');
Route::get('/dailywinnerjson', 'DailyWinnerJsonController@getWinner');
// 111.000 RSD DOBITNIK
Route::get('/izvlacenje-nedeljnog-dobitnika', 'HundredThousandsController@hundredThousandsWinner');
Route::get('/hundredwinnerjson', 'HundredThousandsJsonController@getWinner');
// PUTOVANJE U MADRID
Route::get('/izvlacenje-putovanja-madrid', 'MadridController@madridWinner');
Route::get('/madridwinnerjson', 'MadridJsonController@getWinner');
// PUTOVANJE U LONDON
Route::get('/izvlacenje-putovanja-london', 'LondonController@londonWinner');
Route::get('/londonwinnerjson', 'LondonJsonController@getWinner');
// PUTOVANJE U LA
Route::get('/izvlacenje-putovanja-LA', 'MainWinnerController@mainWinner');
Route::get('/mainwinnerjson', 'MainWinnerJsonController@getWinner');

// ** IZVLACENJE NAGRADA **

// Winners
//Route::get('/dobitnici', 'WinnersListController@getView');

// Contact
Route::get('/kontakt', 'ContactController@getView');

// Rules
Route::get('/pravila', 'RullesController@getView');

// Rewards
Route::get('/nagrade', 'AwardsController@getView');

// Pause
Route::get('/pauza', 'AfterHoursController@getView');

// Game over
Route::get('/zavrsena-igra', 'GameFinishedController@getView');

// Game over
Route::get('/obavestenje-o-dogadjajima', 'EventsController@getView')->name('events');
