<?php

namespace App\Exports;

use App\Winner;
use Maatwebsite\Excel\Concerns\FromCollection as Collection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class WinnersTableExport implements Collection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $winnersExport = Winner::select([
            'id', 'user_id', 'ammount', 'prize_type', 'won_at'
        ])->get();

        return collect($winnersExport);
    }

    public function headings(): array
    {
        return [
            'ID korisnika',
            'Generički kod',
            'Iznos nagrade',
            'Vrsta nagrade',
            'Izvučen'
        ];
    }
}
