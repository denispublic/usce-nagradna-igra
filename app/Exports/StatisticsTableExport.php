<?php

namespace App\Exports;

use App\Statistics;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class StatisticsTableExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $statisticsExport = Statistics::select([
            'date', 'three_thousand', 'six_thousand', 'hundred_thousand', 'madrid_travel', 'london_travel', 'total_users'
        ])->get();


        return collect($statisticsExport);
    }

    public function headings(): array
    {
        return [
            ' ',
            '3.000 rsd',
            '6.000 rsd',
            '111.000 rsd',
            'Madrid-putovanje',
            'London-putovanje',
            'Ukupno prijavljenih',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:E32',
                    [
                        'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                                'color'       => ['argb' => '000'],
                            ],
                        ]
                    ]
                );
            },
        ];
    }
}
