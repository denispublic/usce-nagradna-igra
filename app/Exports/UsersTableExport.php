<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromCollection;

class UsersTableExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $usersExport = User::select([
            'id', 'firstname', 'lastname', 'generated_id', 'email', 'phone_number',
            'bill_number1', 'bill_number2', 'bill_ammount1', 'bill_ammount2',
            'shoping_place', 'shoping_place2', 'award_ammount', 'month', 'day', 'year',
            'newsletter'
        ])->get();

        return collect($usersExport);
    }

    public function headings(): array
    {
        return [
            'ID korisnika',
            'Ime',
            'Prezime',
            'Generički kod',
            'Email',
            'Broj telefona',
            'Broj računa 1',
            'Broj računa 2',
            'Iznos računa 1',
            'Iznos računa 2',
            'Prodavnica 1',
            'Prodavnica 2',
            'Iznos nagrade',
            'Mesec izdavanja računa',
            'Dan izdavanja računa',
            'Godina izdavanja računa',
            'Prihvata obaveštenja'
        ];
    }
}
