<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    public $timestamps = false;

    // Setting relation to user table
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
