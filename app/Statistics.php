<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistics extends Model
{
    protected $fillable = [
        'date',
        'three_thousand',
        'six_thousand',
        'hundred_thousand',
        'madrid_travel',
        'london_travel',
        'total_users',
    ];
}
