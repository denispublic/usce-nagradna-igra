<?php

namespace App\Console\Commands;

use Mail;
use App\Services\GameStatistics;
use App\Exports\StatisticsTableExport;
use App\Exports\UsersTableExport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class FillStatisticsTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fill:statistics_table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill table statistics with processed data';

    private $statistic;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(GameStatistics $statistic)
    {
        parent::__construct();
        $this->statistic = $statistic;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->statistic->insertStatisticData();
        $this->statistic->sendAttachedMail();
    }
}
