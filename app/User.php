<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $timestamps = false;

    // Setting relation to winner table
    public function winner()
    {
        return $this->hasOne('App\Winner');
    }
}
