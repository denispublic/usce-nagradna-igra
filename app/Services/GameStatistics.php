<?php

namespace App\Services;

use Mail;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\User;
use App\Winner;
use App\Statistics;
use App\Exports\StatisticsTableExport;
use App\Exports\UsersTableExport;
use App\Exports\WinnersTableExport;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class GameStatistics
{
    public function insertStatisticData()
    {
        $today = Carbon::now()->toFormattedDateString();

        $totalUsers = User::whereDate('created_at', Carbon::today())
                          ->count();

        $threeKWinners = Winner::whereDate('won_at', Carbon::today())
                               ->where('ammount', 3000)
                               ->count();

        $sixKWinners = Winner::whereDate('won_at', Carbon::today())
                               ->where('ammount', 6000)
                               ->count();

        $hundredKWinners = Winner::whereDate('won_at', Carbon::today())
                               ->where('ammount', 111000)
                               ->count();

        $madridTravel = Winner::whereDate('won_at', Carbon::today())
                               ->where('ammount', 'Madrid')
                               ->count();

        $londonTravel = Winner::whereDate('won_at', Carbon::today())
                               ->where('ammount', 'London')
                               ->count();

        $columns = new Statistics();
        $columns->date = $today;
        $columns->three_thousand = $threeKWinners;
        $columns->six_thousand = $sixKWinners;
        $columns->hundred_thousand = $hundredKWinners;
        $columns->madrid_travel = $madridTravel;
        $columns->london_travel = $londonTravel;
        $columns->total_users = $totalUsers;
        $columns->save();
    }

    public function sendAttachedMail()
    {
        //export of all relevant tables
        $statisticsExport = date("d-m-Y") . '-izveštaj.xlsx';
        Excel::store(new StatisticsTableExport, $statisticsExport);
        $usersExport = date("d-m-Y") . '-korisnici.xlsx';
        Excel::store(new UsersTableExport, $usersExport);
        $winnersExport = date("d-m-Y") . '-dobitnici.xlsx';
        Excel::store(new WinnersTableExport, $winnersExport);

        $files = Storage::allFiles();
        //find of last report
        $searchword = '-izveštaj.xlsx';
        $matchedReport = array_filter($files, function($member) use ($searchword) {
            return preg_match("/\b$searchword\b/i", $member);
        });
        $report = array_diff($matchedReport, [".gitignore", "backups/.gitignore", "public/.gitignore"]);
        $exportReport = array_slice($report, -1, 1, true);

        //find of last users
        $searchword = '-korisnici.xlsx';
        $matchedUsers = array_filter($files, function($member) use ($searchword) {
            return preg_match("/\b$searchword\b/i", $member);
        });
        $users = array_diff($matchedUsers, [".gitignore", "backups/.gitignore", "public/.gitignore"]);
        $exportUsers = array_slice($users, -1, 1, true);

        //find of last winners
        $searchword = '-dobitnici.xlsx';
        $matchedWinners = array_filter($files, function($member) use ($searchword) {
            return preg_match("/\b$searchword\b/i", $member);
        });
        $winners = array_diff($matchedWinners, [".gitignore", "backups/.gitignore", "public/.gitignore"]);
        $exportWinners = array_slice($winners, -1, 1, true);

        //sending emails with three attachments (report, users, winners)

        $mailer = new PHPMailer(true);
        $mailer->setFrom('usce@usce.rs', 'Usce');
        $mailer->addAddress('denis.micijevic@m1.rs', 'Denis Micijevic', 0);
//        $mailer->addAddress('usce@usce.rs', 'Usce', 0); //TODO odkomentarisati pred pocetak igre
        $mailer->isHTML(true);
        $mailer -> charSet = "utf-8";

        $mailer->Subject = 'Dnevni izveštaj - Ušće nagradna igra';
        $mailer->Body = "Izvestaj";

        foreach ($exportReport as $oneReport) {
          $mailer->addAttachment('storage/app/' . $oneReport);
        }
        foreach($exportUsers as $oneUsers) {
          $mailer->addAttachment('storage/app/' . $oneUsers);
        }
        foreach($exportWinners as $oneWinners) {
          $mailer->addAttachment('storage/app/' . $oneWinners);
        }

      if($mailer->send())
      {
        echo "Message has been sent successfully";
      }
      else
      {
        echo "Mailer Error: " . $mailer->ErrorInfo;
      }



//        Mail::send('emails.reportmail', [], function($message) use ($exportReport, $exportUsers, $exportWinners) {
//            //izmeniti i dodati primaoca i sl
//            $message->to('denis.micijevic@m1.rs', 'usce@usce.rs')
//                    ->subject('Dnevni izveštaj - Ušće nagradna igra');
//                foreach ($exportReport as $oneReport) {
//                    $message->attach('storage/app/' . $oneReport);
//                }
//                foreach($exportUsers as $oneUsers) {
//                    $message->attach('storage/app/' . $oneUsers);
//                }
//                foreach($exportWinners as $oneWinners) {
//                    $message->attach('storage/app/' . $oneWinners);
//                }
//        });
    }
}
