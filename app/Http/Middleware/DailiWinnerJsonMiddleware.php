<?php

namespace App\Http\Middleware;

use Closure;

class DailiWinnerJsonMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Checking if session is set and redirecting back if it doesnt
        if(!$request->session()->has('lucky_jack')) {
            return redirect('/');
        }
        
        return $next($request);
    }
}
