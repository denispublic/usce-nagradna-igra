<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;

class BlockGameOverMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentDay = Carbon::now()->format('M-d');
        if($currentDay < 'April-13') {
            return redirect('/');
        } else {
            return $next($request);
        }
    }
}
