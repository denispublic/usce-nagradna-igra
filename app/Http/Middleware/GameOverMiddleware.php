<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;

class GameOverMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentDay = Carbon::now()->format('M-d');
        if($currentDay >= 'March-16') {
            return redirect('/zavrsena-igra');
        } else {
            return $next($request);
        }
    }
}
