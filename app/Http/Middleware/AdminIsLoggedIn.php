<?php

namespace App\Http\Middleware;

use Closure;

class AdminIsLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Redirecting to Dashboard page if admin is already logged in
        if ($request->session()->has('admin_session')) {
            return redirect('/dashboard');
        }

        return $next($request);
    }
}
