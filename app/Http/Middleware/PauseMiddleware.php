<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;

class PauseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentTime = Carbon::now()->format('H:i');
        if(($currentTime >= '06:00') && ($currentTime < '22:00')) {
            return $next($request);
        } else {
            return redirect('/pauza');
        }
    }
}
