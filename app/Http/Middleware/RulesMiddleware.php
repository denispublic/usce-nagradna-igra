<?php

namespace App\Http\Middleware;

use Closure;

class RulesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Checking if user accepted game rules
        if (!$request->session()->has('accepted')) {
            return redirect()->back();
        }

        // Allowing acces to Register page 
        return $next($request);
    }
}
