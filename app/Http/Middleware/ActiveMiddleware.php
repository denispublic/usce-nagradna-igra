<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;

class ActiveMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentTime = Carbon::now()->format('H:i');
        // OVDE MENJAS KADA U TOKU DANA KRECE IGRA
        if(($currentTime >= '10:00') && ($currentTime < '22:00')) {
            return redirect('/');
        } else {
            return $next($request);
        }
    }
}
