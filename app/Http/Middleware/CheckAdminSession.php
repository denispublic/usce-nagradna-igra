<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdminSession 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) 
    {
        // Checking if admin is logged in and restrict access if he is not
        if (!$request->session()->has('admin_session')) {
            return redirect('/login');
        }

        return $next($request);
    }
}
