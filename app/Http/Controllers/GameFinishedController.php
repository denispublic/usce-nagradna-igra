<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GameFinishedController extends Controller
{
    // Checking end date
    public function __construct()
    {
        $this->middleware('blockgameover');
    }
    
    // Returns game over view
    public function getView() {
        return view('pages.gamefinished');
    }
}
