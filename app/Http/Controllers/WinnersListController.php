<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Winner;

class WinnersListController extends Controller {
  // Checking if pause and gameover is active
  public function __construct() {
    $this->middleware('pause');
    $this->middleware('gameover');
  }

  // Return winners list page
  public function getView() {
    // Get all daily winners
    $dailyWinners = Winner::where('prize_type', 'Daily winner')->orderBy('user_id', 'DESC')->get();

    // Get all 10 day winners
    $tenDayWinners = Winner::where('prize_type', '111.000 dobitnik')->orderBy('user_id', "DESC")->get();

    // Get London winner
    $londonWinner = Winner::where('prize_type', 'London putovanje')->orderBy('user_id', 'DESC')->get();

    // Get Madrid winner
    $madridWinner = Winner::where('prize_type', 'Madrid putovanje')->orderBy('user_id', 'DESC')->get();

    // Get main winner
    $mainWinner = Winner::where('prize_type', 'Main winner')->orderBy('user_id', 'DESC')->get();

    $data = [
      'dailyWinners' => $dailyWinners,
      'tenDayWinners' => $tenDayWinners,
      'mainWinner' => $mainWinner,
      'londonWinner' => $londonWinner,
      'madridWinner' => $madridWinner

    ];

    return view('pages.winnerslist')->with($data);
  }
}
