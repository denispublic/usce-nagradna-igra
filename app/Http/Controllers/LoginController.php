<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Middleware\AdminIsLoggedIn;
use App\Admin;
use Hash;
use DB;

class LoginController extends Controller 
{
    // Redirecting to Dashboard page if admin is already logged in
    public function __construct()
    {
        $this->middleware('admin_is_logged_in')->only([
            'getView'
        ]);
    }

    // Return login form view
    public function getView() 
    {
        return view('pages.login');
    }

    // Login user if he enters correct username and password
    public function login(Request $request) 
    {
        // Collecting data from form
        $username = $request->input('username');
        $password = $request->input('password');

        // Form rules
        $rules = [
            'username' => ['required'],
            'password' => ['required']
        ];

        // Error messages
        $messages = [
            'username.required' => 'Morate uneti korisnicko ime!',
            'password.required' => 'Morate uneti lozinku!',
        ];

        // Validating a form
        $request->validate($rules, $messages);

        // Checking if admin username is correct
        $clause = [
            ['username', '=', $username]
        ];
        $admin = DB::table('admins')
                ->where($clause)
                ->first();

        // Checking if admin password is correct and redirecting if it is
        if(Hash::check($password, optional($admin)->password)) {
            // Setting admin sesison
            $value = bcrypt(uniqid());

            $request->session()->put('admin_session', $value);
            return redirect('/dashboard');
        } else {
            return redirect()
                ->back()
                ->with('error', 'Neispravno korisničko ime ili lozinka!');
        }
    }
}
