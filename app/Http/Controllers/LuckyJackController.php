<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Middleware\CheckSessionForLuckyJack;
use App\Http\Middleware\DailiWinnerJsonMiddleware;

class LuckyJackController extends Controller
{
    // Checking if user came to Lucky Jack page properly
    public function __construct()
    {
        $this->middleware('check_lucky_jack_session');
        $this->middleware('lucky_jack');
    }

    // Return Lucky Jack view and getting user id from session
    public function getView(Request $request)
    {
        // Deleting user id session and blocks him to play lucky jack more then once
        $request->session()->forget('lucky_jack');

        return view('pages.luckyjack');
    }
}
