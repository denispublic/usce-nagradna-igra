<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RullesController extends Controller
{
    // Checking if pause and gameover is active
    public function __construct()
    {
        $this->middleware('pause');
        $this->middleware('gameover');
    }

    // Returns Rules Page view
    public function getView() 
    {
        return view('pages.rules');
    }
}
