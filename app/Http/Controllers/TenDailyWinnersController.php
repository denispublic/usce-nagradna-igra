<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Winner;
use App\User;
use DB;
use File;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


class TenDailyWinnersController extends Controller
{
    // Check if admin is logged in
    public function __construct()
    {
        $this->middleware('check_admin_session');
    }

    public function tenDayWinners(Request $request)
    {
        // Setting price
        $ammount = '100000';

        // Getting current time
        $carbon = Carbon::now();

        // Time for insert
        $now = $carbon->toDateTimeString();

        // Date for check
        $dateCheck = $carbon->format('Y-m-d');

        // Getting non winners
        $clause = [
            ['winner', '=', 0]
        ];
        $users = DB::table('users')->where($clause)->get();

        // Get main winner and change his value in winners
        if($users->count() != 0){
            // Checking if winner already exists
            $condition = [
                ['prize_type', '=', '10 day winner'],
                ['won_at', 'like', $dateCheck . '%']
            ];
            $winnerExist = DB::table('winners')->where($condition)->get();
            if($winnerExist->count() > 0) {
                return redirect()->back()->with('error', 'Desetodnevni dobitnik je već izabran. Sledeće izvlačenje je moguće za deset dana!');
            } else {
                $user = DB::table('users')->where($clause)->inRandomOrder()->limit(1)->first();
                $user_id = $user->id;
                //PROVERI
                $user_email = $user->email;
                $user_firstname = $user->firstname;
                //PROVERI
                DB::table('users')->where('id', $user_id)->update(['winner' => 1]);
                $tenDailyWinner = new Winner;
                $tenDailyWinner->user_id = $user_id;
                $tenDailyWinner->ammount = $ammount;
                $tenDailyWinner->prize_type = '10 day winner';
                $tenDailyWinner->won_at = $now;
                $tenDailyWinner->save();

                //sending mail to winner
                $mailer = new PHPMailer(true);
                $mailer->setFrom('usce@usce.rs', 'Usce');
                $mailer->addAddress($user_email, $user_firstname, 0);
                $mailer->isHTML(true);
                $mailer->Subject = 'Ušće - nagrada';
                $mailer->Body = str_replace(':percent:', '%', sprintf(File::get(app_path() . '/Mails/awardsmail.php'), number_format($ammount, '2', ',', '.')));
                $mailer->send();

                // Setting user session for json
                $request->session()->put('tendailywinner', $user);
            }
        } else {
            return redirect()->back()->with('error', 'Akciju je nemoguće izvršiti!');
        }

        return view('pages.tendaywinnerrotator')->with('user', $user);
    }
}
