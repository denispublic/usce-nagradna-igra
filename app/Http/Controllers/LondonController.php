<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Winner;
use App\User;
use DB;
use File;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class LondonController extends Controller
{
    // Check if admin is logged in
    public function __construct()
    {
        $this->middleware('check_admin_session');
    }

    public function londonWinner(Request $request)
    {
        // Setting price
        $ammount = 'London';

        // Getting current time
        $carbon = Carbon::now();

        // Time for insert
        $now = $carbon->toDateTimeString();

        // Date for check
        $dateCheck = $carbon->format('Y-m-d');

        // UZIMANJE SVIH KORISNIKA IZ KOLEKCIJE USERS KOJI NISU DOBITNICI
        $clause = [
            ['winner', '=', 0]
        ];
        $users = DB::table('users')->where($clause)->get();

        // Get main winner and change his value in winners
        if($users->count() != 0){
            // Checking if winner already exists
            $condition = [
                ['prize_type', '=', 'London putovanje'],
                ['won_at', 'like', $dateCheck . '%']
            ];
            $winnerExist = DB::table('winners')->where($condition)->get();
            if($winnerExist->count() > 10) {
                return redirect()->back()->with('error', 'Dobitnik nagrade putovanje za London je već izabran!');
            } else {
                $user = DB::table('users')->where($clause)->inRandomOrder()->limit(1)->first();
                $user_id = $user->id;
                //PROVERI
                $user_email = $user->email;
                $user_firstname = $user->firstname;
                //PROVERI
                DB::table('users')->where('id', $user_id)->update(['winner' => 1]);
                $tenDailyWinner = new Winner;
                $tenDailyWinner->user_id = $user_id;
                $tenDailyWinner->ammount = $ammount;
                $tenDailyWinner->prize_type = 'London putovanje';
                $tenDailyWinner->won_at = $now;
                $tenDailyWinner->save();

                // SLANJE MEJLA DOBITNIKU PUTOVANJE U LONDON *OTKOMENTARISATI NA SERVERIMA*
//                $mailer = new PHPMailer(true);
//                $mailer->setFrom('usce@usce.rs', 'Usce');
//                $mailer->addAddress($user_email, $user_firstname, 0);
//                $mailer->isHTML(true);
//                $mailer->Subject = 'Usce - nagrada';
//                $mailer->Body = str_replace(':percent:', '%', sprintf(File::get(app_path() . '/Mails/londonmail.php')));
//                $mailer->send();
                // SLANJE MEJLA DOBITNIKU PUTOVANJE U LONDON *OTKOMENTARISATI NA SERVERIMA*

                // Setting user session for json
                $request->session()->put('izvlacenje-putovanja-london', $user);
            }
        } else {
            return redirect()->back()->with('error', 'Akciju je nemoguće izvršiti!');
        }

        return view('pages.londonrotator')->with('user', $user);
    }
}
