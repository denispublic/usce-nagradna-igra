<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AfterHoursController extends Controller
{
    // Check if pause is zctive
    public function __construct()
    {
        $this->middleware('active');
    }
 
    // Returns pause view
    public function getView() {
        return view("pages.afterhours");
    }
}
