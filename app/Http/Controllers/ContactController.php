<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    // Checking if pause and gameover is active
    public function __construct()
    {
//        $this->middleware('pause');
//        $this->middleware('gameover');
    }

    // Returns Contact page view
    public function getView()
    {

//         $admin = new Admin;
//         $admin->username = 'admin';
//         $admin->password = bcrypt('2019uscebg');
//         $admin->save();

        return view('pages.contact');
    }
}
