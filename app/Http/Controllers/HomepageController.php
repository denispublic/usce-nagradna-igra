<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomepageController extends Controller
{
    // Checking if pause and gameover is active
    public function __construct()
    {
        $this->middleware('pause')->only([
            'getView'
        ]);
        $this->middleware('gameover')->only([
            'getView'
        ]);
    }

    // Return Homepage view
    public function getView()
    {
        return view('pages.homepage');
    }

    // Checking if user accepted terms and conditions
    public function terms(Request $request)
    {
        // Validating a form
        $rules = [
            'terms-and-conditions' => ['required'],
            'newsletter' => ['boolean'],
        ];
        $messages = [
            'terms-and-conditions.required' => 'Morate prihvatiti pravila igre'
        ];
        $request->validate($rules, $messages);

        // Setting sessiom for register page
        $value = bcrypt(microtime());
        $request->session()->put('accepted', $value);


        if($request->newsletter !== NULL && $request->newsletter !== 0) {
            $request->session()->put('newsletter', 1);
        } else {
            $request->session()->put('newsletter', 0);
        }
        
        return redirect('/prijava');
    }
}

