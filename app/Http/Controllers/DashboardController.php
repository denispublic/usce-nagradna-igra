<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Middleware\CheckAdminSession;
use DB;

class DashboardController extends Controller
{
    // Allow access to page only if admin sesison is set
    public function __construct() 
    {
        $this->middleware('check_admin_session');
    }

    // Returns admin page view
    public function getView() 
    {
        // Get all registerd users
        $users = DB::table('users')
            ->leftJoin('winners', 'users.id', '=', 'winners.user_id')
            ->select('*')->get();

        return view('pages.dashboard')->with('users', $users);
    }
}
