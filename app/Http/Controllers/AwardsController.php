<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AwardsController extends Controller
{
    // Checking if pause and gameover is active
    public function __construct()
    {
        $this->middleware('pause');
        $this->middleware('gameover');
    }

    // Returns Awards page view
    public function getView() 
    {
        return view('pages.awards');
    }
}
