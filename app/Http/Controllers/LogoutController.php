<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LogoutController extends Controller
{
    // Admin logout
    public function logout(Request $request) 
    {
        // Deleting admin session and redirecting him to login page
        $request->session()->forget('admin_session');
        return redirect('/login');
    }
}
