<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Middleware\CheckSessionForLuckyJack;

class DailyWinnerJsonController extends Controller
{
    // Block access to json 
    public function __construct()
    {
        $this->middleware('check_lucky_jack_session');
    }

    // Get winner in json format
    public function getWinner(Request $request)
    {
        // Getting user id stored in session
        // u zagradi pre deklaracije moze da se kastuje
        $user_id = $request->session()->get('user_id');
        $user_generated_id = $request->session()->get('generated_id');

        // Getting current user
        $clause = [
            ['user_id', '=', $user_id]
        ];
        $winner = DB::table('winners')->where($clause)->first() ?: $user_generated_id;

        // Deleting user id session and blocks him to play lucky jack more then once
        $request->session()->forget('user_id');

        // VRACA NA RUTU: /dailywinnerjson
        return response()->json($winner);
    }
}
