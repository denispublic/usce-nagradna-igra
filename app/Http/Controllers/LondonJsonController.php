<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LondonJsonController extends Controller
{
    // Allow access to page only if admin sesison is set
//    public function __construct()
//    {
//        $this->middleware('check_admin_session');
//    }

    public function getWinner(Request $request)
    {
        // Getting encoded user stored in session
        $winner = $request->session()->get('izvlacenje-putovanja-london');

        return response()->json($winner);
    }
}
