<?php
namespace App\Http\Controllers;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Illuminate\Http\Request;
use App\Rules\FirstnameUppercase;
use App\Rules\LastnameUppercase;
use App\Rules\PhoneValidation;
use App\User;
use App\Winner;
use Carbon\Carbon;
use DB;
use File;
use App\Http\Middleware\RulesMiddleware;
use Illuminate\Validation\Rule;


class RegisterController extends Controller
{
    // Checking if user accepted game rules
    public function __construct()
    {
        $this->middleware('accepted')->only([
            'getView'
        ]);
    }

    // Returns Register View
    public function getView()
    {
        return view('pages.register');
    }

    // Validate input fields and store user in database
    public function register(Request $request)
    {
        $accepted = session()->get('newsletter');
        if($accepted !== null) {
            echo $accepted;
        }
        // Get values from input fields
        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        $phone = $request->input('phone');
        $email = $request->input('email');
        $bill_number = $request->input('bill_number1');
        $bill_number2 = $request->input('bill_number2');
        $bill_ammount = $request->input('bill_ammount1');
        $bill_ammount2 = $request->input('bill_ammount2');
        $shoping_place = $request->input('shoping_place');
        $shoping_place2 = $request->input('shoping_place2');
        $shopping_year = $request->input('year');
        $shopping_day = $request->input('day');
        $shopping_month = $request->input('month');
        $shopping_year2 = $request->input('year_two');
        $shopping_day2 = $request->input('day_two');
        $shopping_month2 = $request->input('month_two');

        // Validate form data
        $rules = [
            'firstname' => ['required', 'alpha', new FirstnameUppercase],
            'lastname' => ['required', 'alpha', new LastnameUppercase],
            'phone' => ['required', 'numeric', new PhoneValidation],
            'email' => ['required', 'email'],
            'bill_number1' => ['required', Rule::unique('users')
                           ->where(function($query) use($bill_number, $shoping_place) {
                           return $query->where('bill_number1', $bill_number)
                           ->where('shoping_place', $shoping_place);
            })],
            'bill_number2' => ['nullable', 'numeric'],
            'bill_ammount1' => ['required', 'numeric'],
            'bill_ammount2' => ['nullable', 'numeric'],
            'shoping_place' => ['required'],
            'shoping_place2' => ['nullable'],
            'year' => ['required'],
            'month' => ['required'],
            'day' => ['required'],
            'year_two' => ['nullable'],
            'month_two' => ['nullable'],
            'day_two' => ['nullable'],
        ];
        $messages = [
            'firstname.required' => 'Morate uneti Vaše ime!',
            'firstname.alpha' => 'Ime može sadržati samo slova!',
            'lastname.required' => 'Morate uneti Vaše prezime!',
            'lastname.alpha' => 'Prezime može sadržati samo slova!',
            'phone.required' => 'Morate uneti Vaš broj telefona!',
            'phone.numeric' => 'Broj telefona može sadržati samo brojeve!',
            'email.required' => 'Morate uneti Vašu e-mail adresu!',
            'email.email' => 'Email adresa nije validna!',
            'bill_number1.required' => 'Ovo polje je obavezno!',
            'bill_number1.unique' => 'Račun iz ove prodavnice već postoji!',
            'bill_number1.numeric' => 'Unesite samo brojeve!',
            'bill_number2.numeric' => 'Unesite samo brojeve!',
            'bill_number2.unique' => 'Račun već postoji!',
            'bill_ammount1.required' => 'Ovo polje je obavezno!',
            'bill_ammount1.numeric' => 'Unesite samo brojeve!',
            'bill_ammount2.numeric' => 'Unesite samo brojeve!',
            'shoping_place.required' => 'Ovo polje je obavezno!',
            'year.required' => 'Ovo polje je obavezno!',
            'year.numeric' => 'Morate uneti odgovarajuću vrednost!',
            'month.required' => 'Ovo polje je obavezno!',
            'month.numeric' => 'Morate uneti odgovarajuću vrednost!',
            'day.required' => 'Ovo polje je obavezno!',
            'day.numeric' => 'Morate uneti odgovarajuću vrednost!',
            'year_two.numeric' => 'Morate uneti odgovarajuću vrednost!',
            'month_two.numeric' => 'Morate uneti odgovarajuću vrednost!',
            'day_two.numeric' => 'Morate uneti odgovarajuću vrednost!',
        ];
        $request->validate($rules, $messages);

        // Generate random time
        $random_time = mt_rand(10,22).":".str_pad(mt_rand(0,59), 2, "0", STR_PAD_LEFT);

        // Generate created at time
        $carbon = Carbon::now();
        $now = $carbon->toDateTimeString();

        // Difference between generated and created at time
        $diff = abs(strtotime($now) - strtotime($random_time));

        // Generate user generated id
        $generated_id = substr(str_shuffle("01234567890"), 0, 12);

        // Setting array of prices
//        $random_prices = ['3000' => '3000','6000' => '6000', '100000' => '100000'];
        $random_prices = ['3000' => '3000','6000' => '6000'];

        // Unseting 3000 price if limit is exceeded
        $limit3000 = DB::table('winners')->where([
            ['prize_type', '=', 'Dnevni dobitnik'],
            ['ammount', '=', '3000']
        ]);
        //ispod se podesava limit za broj nagrada od 3000
        if($limit3000->count() >= 111) {
            unset($random_prices['3000']);
        }

        // Unseting 6000 price if limit is exceeded
        $limit6000 = DB::table('winners')->where([
            ['prize_type', '=', 'Dnevni dobitnik'],
            ['ammount', '=', '6000']
        ]);
        //ispod se podesava limit za nagrade od 6000
        if($limit6000->count() >= 111) {
            unset($random_prices['6000']);
        }

        // ISPOD TI JE KOMENTARISAN KOD ZA 100.000 JER JE TO RANIJE BILA DNEVNA NAGRADA
        // Unseting 12000 price if limit is exceeded
//        $limit100000 = DB::table('winners')->where([
//            ['prize_type', '=', 'Daily winner'],
//            ['ammount', '=', '100000']
//        ]);
//        if($limit100000->count() >= 10) {
//            unset($random_prices['100000']);
//        }

        // Checking if bill amount is higher then 5000
        if($bill_ammount >= 5000 || ($bill_ammount + $bill_ammount2) >= 5000) {
            // Cuvanje usera u tabelu korisnici
            $user = new User;
            $user->firstname = $firstname;
            $user->lastname = $lastname;
            $user->generated_id = $generated_id;
            $user->email = $email;
            $user->bill_number1 = $bill_number;
            $user->bill_number2 = $bill_number2;
            $user->bill_ammount1 = $bill_ammount;
            $user->bill_ammount2 = $bill_ammount2;
            $user->shoping_place = $shoping_place;
            $user->shoping_place2 = $shoping_place2;
            $user->phone_number = $phone;
            $user->year = $shopping_year;
            $user->month = $shopping_month;
            $user->day = $shopping_day;
            $user->year_two = $shopping_year2;
            $user->month_two = $shopping_month2;
            $user->day_two = $shopping_day2;
            // ISPOD TI JE FREKVENTNOST DNEVNIH DOBITAKA, STO JE VECI BROJ TO JE VISE DOBITNIKA
            // izmeni razliku u 1800/ iz 45000 u 1800 da bi ivlacio mali broj dnevnih dobitnika
            // AKO HOCES DA SVAKI DOBIJE ONDA TI JE 45000, AKO HOCES DA NISTA NE DOBIJE STAVI 10, IZMEDJU TI JE 1000
            $user->winner = ($diff <= 10) ? 1 : 0;
            // IZNAD  TI JE FREKVENTNOST DNEVNIH DOBITAKA, STO JE VECI BROJ TO JE VISE DOBITNIKA
            $user->created_at = $now;
            $accepted = session()->get('newsletter');
            $user->newsletter = $accepted;
            $user->save();

            // SLANJE MEJLA KAD SE REGISTRUJE

//             $mailer = new PHPMailer(true);
//             $mailer->setFrom('usce@usce.rs', 'Ušće');
//             $mailer->addAddress($email, $firstname, 0);
//             $mailer->isHTML(true);
//             $mailer->Subject = 'Usce';
//             $mailer->Body = str_replace(':percent:', '%', sprintf(File::get(app_path() . '/Mails/registermail.php'), $generated_id));
//             $mailer->AltBody = 'Vaš jedinstveni registracioni broj je ' . $generated_id;
//             $mailer->send();

            // Storing user in database if winner
            $clause = [
                ['winner', '=', 1],
                ['id', '=', $user->id]
            ];
            //cuvanje usera u tabelu winner ako je dobitnik
            $users = DB::table('users')->where($clause)->get();
            if($users->count() === 1) {
                if(!empty($random_prices)) {
                    $ammount = array_rand($random_prices);
                    $winner = new Winner;
                    $winner->user_id = $user->id;
                    $winner->ammount = $ammount;
                    $winner->prize_type = 'Dnevni dobitnik';
                    $winner->won_at = $now;
                    $winner->save();
                    //update users tabele sa vrednoscu iznosa nagrade
                    $user->award_ammount = $winner->ammount;
                    $user->update();

                    // OVO ISPOD JE MEJL KOJI TREBA DA SE TESTIRA NA SERVERIMA (DEV I LIVE)

                    // Sending mail to user if he won a price

                    // $mailer = new PHPMailer(true);
                    // $mailer->setFrom('usce@usce.rs', 'Ušće');
                    // $mailer->addAddress($email, $firstname, 0);
                    // $mailer->isHTML(true);
                    // $mailer->Subject = 'Usce - nagrada';
                    // $mailer->Body = str_replace(':percent:', '%', sprintf(File::get(app_path() . '/Mails/awardsmail.php'), number_format($ammount, '2', ',', '.')));
                    // $mailer->AltBody = 'Dobili ste nagradu u iznosu od ' . $ammount . ' RSD';
                    // $mailer->send();

                    // OVO IZNAD JE MEJL KOJI TREBA DA SE TESTIRA NA SERVERIMA (DEV I LIVE)

                } else {
                    $user->winner = 0;
                    $user->save();
                }
            }

            // Storing user id into session
            $request->session()->put('user_id', $user->id);
            $request->session()->put('generated_id', $user->generated_id);
            $request->session()->put('lucky_jack', $user->id);

            // Detete rules session
            $request->session()->forget('accepted');

            return redirect('igra');
        } else {
            return redirect()->back()->with('error', 'Iznos na ručunu/računima mora biti veći od 5.000,00 RSD')->withInput();
        }
    }
}
