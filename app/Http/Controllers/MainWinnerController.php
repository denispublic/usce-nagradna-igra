<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Winner;
use DB;
use File;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class MainWinnerController extends Controller
{
    // Check if admin is logged in
    public function __construct()
    {
        $this->middleware('check_admin_session');
    }

    public function mainWinner(Request $request)
    {
        // Setting ammount
        $ammount = 5000;

        // Getting current time
        $carbon = Carbon::now();
        $now = $carbon->toDateTimeString();

        // Getting non winners
        $clause = [
            ['winner', '=', 0]
        ];
        $users = DB::table('users')->where($clause)->get();

        // Get main winner and change his value in winners
        if($users->count() != 0){
            // Checking if main winner already exists
            $condition = [
                ['prize_type', '=', 'Main winner']
            ];
            $winnerExist = DB::table('winners')->where($condition)->get();
            if($winnerExist->count() > 10) {
                return redirect()->back()->with('error', 'Glavni dobitnik je već izabran!');
            } else {
                $user = DB::table('users')->where($clause)->inRandomOrder()->limit(1)->first();
                $user_id = $user->id;

                //PROVERI
                $user_email = $user->email;
                $user_firstname = $user->firstname;
                //PROVERI
                DB::table('users')->where('id', $user_id)->update(['winner' => 1]);
                $mainWinner = new Winner;
                $mainWinner->user_id = $user_id;
                $mainWinner->ammount = $ammount;
                $mainWinner->prize_type = 'Main winner';
                $mainWinner->won_at = $now;
                $mainWinner->save();

                //sending mail to winner
                // $mailer = new PHPMailer(true);
                // $mailer->setFrom('usce@usce.rs', 'Usce');
                // $mailer->addAddress($user_email, $user_firstname, 0);
                // $mailer->isHTML(true);
                // $mailer->Subject = 'Ušće - nagrada';
                // $mailer->Body = str_replace(':percent:', '%', sprintf(File::get(app_path() . '/Mails/mainwinnersmail.php')));
                // //$mailer->AltBody = 'Dobili ste nagradu u iznosu od ' . $ammount . ' RSD';
                // $mailer->send();


                // Setting user session for json
                $request->session()->put('izvlacenje-putovanja-LA', $user);
            }
        } else {
            return redirect()->back()->with('error', 'Akciju je nemoguće izvršiti!');
        }

        return view('pages.mainwinnerrotator')->with('user', $user);
    }
}
